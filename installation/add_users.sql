--
-- Add test users to database
--

CREATE TEMPORARY TABLE tmp_user (
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL
);
source users.sql;

INSERT INTO user (first_name, last_name, email, password,
  password_expiry_date, created_date, updated_date,
  user_level, user_type, user_owner_id, hasWelcomeScreen)
 SELECT first_name, last_name, email,
  '$2y$04$noPassword0123456789n.5oYZwnBu4hYx0.3xqYAglCuEhc9tA3i', '0000-00-00 00:00:00', CURDATE(), CURDATE(), 3, 3, 1, 1
 FROM tmp_user;
DROP TABLE tmp_user;

INSERT INTO cgroup (name, has_budgetf, budgetf_unit_id)
 SELECT CONCAT(first_name, ' ', last_name, ' <', email, '>'), 0, 1
 FROM user WHERE cgroup_id=1 AND id!=1;

UPDATE user AS u
  JOIN cgroup AS g
  ON g.name=CONCAT(u.first_name, ' ', u.last_name, ' <', u.email, '>')
   SET u.cgroup_id=g.id;

INSERT INTO cgroup_category (cgroup_id, category_id, corder)
 SELECT g.id, gc.category_id, gc.corder
 FROM cgroup AS g
 INNER JOIN cgroup_category AS gc ON gc.cgroup_id=1
 LEFT JOIN cgroup_category AS gc2 ON gc2.cgroup_id=g.id
 WHERE gc2.cgroup_id IS NULL AND g.id!=1;

INSERT INTO user_tag (user_id, tag_id)
 SELECT u.id, ut.tag_id
 FROM user AS u
 INNER JOIN user_tag AS ut ON ut.user_id=1
 LEFT JOIN user_tag AS ut2 ON ut2.user_id=u.id
 WHERE ut2.user_id IS NULL AND u.id!=1;

INSERT INTO user_contact (user_id, contact_id)
 SELECT u1.id, u2.id
 FROM user AS u1 CROSS JOIN user AS u2
  LEFT JOIN user_contact AS uc
  ON uc.user_id=u1.id AND uc.contact_id=u2.id
 WHERE uc.user_id IS NULL AND u1.id!=u2.id;
