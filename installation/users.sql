/*
 * List of test users to be added by the sql script add_users.sql
 */

INSERT INTO tmp_user (first_name, last_name, email) VALUES
 ('Alexa',   'Hansen',   'alexa.hansen@requestdetail.net'),
 ('Andrew',  'Abrams',   'andrew.abrams@requestdetail.net'),
 ('Bill',    'Koso',     'bill.koso@requestdetail.net'),
 ('Brock',   'Kenny',    'brock.kenny@requestdetail.net'),
 ('Cynthia', 'Khalsa',   'cynthia.khalsa@requestdetail.net'),
 ('Donald',  'Gorman',   'donald.gorman@requestdetail.net'),
 ('Gina',    'Robinson', 'gina.robinson@requestdetail.net'),
 ('Helen',   'Davis',    'helen.davis@requestdetail.net'),
 ('Janet',   'Baker',    'janet.baker@requestdetail.net'),
 ('Karen',   'Tobin',    'karen.tobin@requestdetail.net'),
 ('Peggy',   'Benedum',  'peggy.benedum@requestdetail.net'),
 ('Steve',   'Austin',   'steve.austin@requestdetail.net'),
 ('Tom',     'Price',    'tom.price@requestdetail.net');
