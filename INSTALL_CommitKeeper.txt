How to install CommitKeeper on a Linux server
=============================================

0) Pre-installation requirements
--------------------------------

To run CommitKeeper, you will need  the following:

- A web-server with PHP installed on the Linux machine.
  PHP must be at least version 5.3.3 and have the modules curl, openssl and gd.

- A MySql database. The MySql database does not need to be on the same
server as the web-server.

In order to download and install all the source code on the web-server,
you will also need the following programs:

- A stand-alone PHP console command
  You need to be able to run PHP from the command line separate from
  the web-server.

- Bazaar revison control
  The CommitKeeper source code is kept in a Bazaar repository.
  Information about downloading Bazaar can be found here:

     http://wiki.bazaar.canonical.com/Download


- Git revision control
  The Symfony 2 framework uses a version control system called
  Git. To get the Symfony 2 source code you will need to have Git installed.

     http://git-scm.com/download/linux


- Composer
  This is a PHP program that makes it simple to download all the code for
  the Symfony 2 framework. Download and install it with the following command:

     curl -s http://getcomposer.org/installer | php

  Move the resulting binary "composer.phar" to somewhere in your PATH,
  (or just remember where you put it for when you need it later).

With all this done, you should be ready to install CommitKeeper on your
web server.


1) Get the CommitKeeper source code
-----------------------------------

If you have not done so already, check out a copy of the CommitKeeper
source code from its Bazaar repository. Terje Bråten has one such
repository of the CommitKeeper source code. To get a copy from his site,
use the following command on your web server:

    bzr branch bzr+ssh://braten@scp.domeneshop.no/ CommitKeeper

The last word "CommitKeeper" in the above command is the name of the
directory where your copy of the source code will live. You can change
that to whatever directory name you want to use. In the rest of this
installation guide, that directory will be referred to as the CommitKeeper
directory, or the CommitKeeper root directory.


2) Make the paramters.yml file
------------------------------

CommitKeeper needs the configuration file app/config/parameters.yml
(All file names in this document are relative to the CommitKeeper root
directory, unless otherwise stated.)
This file tells the program how to log on to the database server and
contains other configuration information that should be local to each
installation.

You can make this configuration file by copying the example file
app/config/parameters.yml.dist to app/config/parameters.yml.
Edit the copied file, and remember to change the secret string of
characters at the end of the file to somthing random that no potential
web virus makers can guess.


3) Make a web/version.php file
-------------------------------

The file web/version.php tells CommitKeeper which version that is
installed, so that information can be displayed in the application.
The file can be created by running this command from the CommitKeeper
directory:

  bzr version-info --custom --template="<?php\n\$version=array('ver'=>'X.x', 'revno'=>'{revno}', 'date'=>'{date}');\n" > web/version.php

(Replace X.x with a version number.)

This file should be updated every time the source code is updated.

4) Get the Symfony 2 framework
------------------------------

The PHP code for the Symfony 2 framework also has to be installed on your
server. Symfony 2 uses Composer to manage itself and its dependencies.
To install it, make sure the CommitKeeper directory is your current directory,
then issue the following command:

    composer.phar install

Then go and get yourself a cup of coffee while you wait for Composer to
install Symfony 2 and its dependencies in the vendor directory.


5) Set file permissions for the writable directories
----------------------------------------------------

The web server (or its php engine) has to be able to write
files in the directories app/cache, app/logs, app/attachments and app/pictures.

For more information about how to do this take a look at this link:
http://symfony.com/doc/2.0/book/installation.html#configuration-and-setup
Look for the heading "Setting up Permissions".

Example commands (may not be correct for your server, see above link.)
  setfacl -R -m u:wwwrun:rwx -m u:`whoami`:rwx app/cache app/logs
  setfacl -dR -m u:wwwrun:rwx -m u:`whoami`:rwx app/cache app/logs


6) Checking your System Configuration
-------------------------------------

Once Symfony 2 is installed, you should make sure that your web server system
is properly configured to run it.

Go to the CommitKeeper directory and execute the "check.php" script
from the command line:

    php app/check.php

When you have done that, make sure that you also access the "check.php" script
from a web browser:

    http://localhost/path/to/CommitKeeper/app/check.php

If you get any warnings or recommendations, fix them before moving on.


7) Install the database
-----------------------

CommitKeeper needs a MySql database that will contain its data.
A mysqldump file to get you started with the database can be found
in installation/initial_database.sql

Start up a MySql client and create an empty database.
Then you can pipe the above file to the "mysql" command,
or use the "source" command from within the MySql client.
A pipe command would look like something like this:

  cat installation/initial_database.sql | mysql -hhostame -uuser -ppasswd dbname

In the installation directory you can also find the file add_users.sql. Source
that file in mysql if you want the test users listed in users.sql to be imported
into the database.

8) Set up cronjobs
------------------

CommitKeeper depends on a cronjob to update all requests that have gone past
their due date, and also another cronjob to send daily updates.
To set up these cronjobs, run the file

 installation/make_cron.sh


9) Install images
-----------------

The images for the CommitKeeper application are not stored in the
source code repository. They have to be installed separately into the
folder web/images. The images are stored in a tar file at http://ck.braten.be/images.tar.gz

10) Install css and javascript files
-----------------------------------

The css and javascript files must be extracted from the source tree
and put in the directories web/css and web/js. This is done by running
the following command in the CommitKeeper directory:

    php app/console assetic:dump --env=prod --no-debug

This command must also be run every time the source code is updated, together
with the version update command described in step 3.


11) Make the web server access web/app.php by default
-----------------------------------------------------

If you use the Apache web server, by default it serves files from
the /var/www directory. You need to set it up to serve files from
the directory /path/to/CommitKeeper/web/ instead.
(One way to do it is to make /var/www a symlink to the
CommitKeeper web directory. Or you can change the configuration of
the Apache server to go directly to the CommitKeeper/web directory.)

In addition to that, the web server should default to always access
the file app.php in that directory for any route it cannot find an
actual file for. This can be done in Apache with the rewrite module.

One way is to put this in a .htaccess file in the "web" directory:
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ app.php [QSA,L]
</IfModule>

Another way is to put the above lines into the Apache config file.
Here is an example config file taken from my Apache2 server:
<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>

        DocumentRoot /home/terje/CommitKeeper/web

        <Directory /home/terje/CommitKeeper/web/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Order allow,deny
                allow from all
                RewriteEngine On
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteRule ^(.*)$ app.php [QSA,L]
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

After a restart of your Apache server, you should then be able to see the
the CommitKeeper login page.


12) Setup OpenId login with Janrain Engage
------------------------------------------

You need to have a Janrain Engage account, you can get one here
  https://rpxnow.com/signup_landing_basic

At the Application Settings page you need to whitelist the domain used
for CommitKeeper.

You will also need to copy the API Key and insert it into the file
CommitKeeper/app/config/security.yml. It should be after
the keyword "janrain_api_key:" in the form_login_ck section.
Structure of the security.yml file:

security:
  firewalls:
    secured_area:
      form_login_ck:
        janrain_api_key: JANRAIN-API-KEY-GOES-HERE

The file
CommitKeeper/src/FSpires/CommitKeeperBundle/Resources/public/js/login/jainrain.js
will also have to be edited/replaced to reflect the janrain sub-domain
you are using.
