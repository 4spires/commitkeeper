function add_new_tag() {
    var tagsDiv = $('#tags_div');
    var last_p = $('#tagp_new');
    var prototype = tagsDiv.attr('data-prototype-name');
    var newIndex = tagsDiv.find('p').length - 1;
    var newForm = $(prototype.replace(/__name__/g, newIndex));
    var trDelete = Translator.trans('tag.delete');
    var newP = $('<p data-tagsource="0"><a class="buttonlink" href="javascript:;" onclick="delete_tag(this);">'+trDelete+'</a></p>').append(newForm);
    last_p.before(newP);
    newForm.focus();
}

function add_old_tag(link) {
    var from_p = $(link.parentNode);
    var id = from_p.attr('id').substring(4);
    var source = from_p.attr('data-tagsource');
    var tagsDiv = $('#tags_div');
    var last_p = $('#tagp_new');
    var addP = tagsDiv.find('#tagp'+id);
    if (addP.length>0) {
        addP.show();
    } else {
        addP = $('<p id="tagp'+id+'" data-tagsource="'+source+'"></p>');
        last_p.before(addP);
    }

    var origContent = from_p.data('origContent');
    if (origContent) {
        addP.append(origContent);
    } else {
        var name = from_p.contents().filter(
                    function() { return this.nodeType == 3; }).text();
        name = document.createTextNode($.trim(name));
        var prototypeId = tagsDiv.attr('data-prototype-id');
        var newIndex = tagsDiv.find('p').length - 1;
        var newIdForm = $(prototypeId.replace(/__name__/g, newIndex));
        newIdForm.val(id);
        var trDelete = Translator.trans('tag.delete');
        var anchor = $('<a class="buttonlink" href="javascript:;" onclick="delete_tag(this)">'+trDelete+'</a>');
        addP.append(newIdForm, anchor, name);
    }

    if (source!=0) {
        from_p.hide();
    } else {
        from_p.remove();
    }
}

function delete_tag(link) {
    var from_p = $(link.parentNode);
    var id = from_p.attr('id');
    if (id) {
        id = id.substring(4);
    }
    if (id) {
        var origContent = from_p.contents();

        var source = from_p.attr('data-tagsource');
        var toDiv = $('#other_tags');

        var addP = toDiv.find('#tagp'+id);
        if (addP.length>0) {
            addP.show();
        } else {
            var trAdd = Translator.trans('tag.add');
            addP = $('<p id="tagp'+id+'" data-tagsource="'+source+'"><a class="buttonlink" href="javascript:;" onclick="add_old_tag(this)">'+trAdd+'</a></p>');
            var name = '';
            var inputfield = from_p.find('input');
            if (inputfield.length>1) {
                name = inputfield.eq(1).val();
            } else {
                name = from_p.find('span').text();
            }
            name = document.createTextNode($.trim(name));
            addP.append(name);
            toDiv.append(addP);
        }
        from_p.hide();
        origContent.detach();
        addP.data('origContent', origContent);
    } else {
        from_p.remove();
    }
}
