var currentActionAutoText = '';
function change_action(action, label, autotext) {
    var classInput = 'hide',
        classSpan  = 'show';
    switch(action) {
    case 'Amend':
    case 'AmendToDo':
    case 'Revise':
        classInput = 'show';
        classSpan  = 'hide';
        break;
    }
    $('#showNewDate').attr("class", classInput);
    $('#showDueDate').attr("class", classSpan);
    $('#showNewBudget').attr("class", classInput);
    $('#showBudget').attr("class", classSpan);
    if (HasStatusUpdate) {
        switch(action) {
        case 'Comment':
        case 'AmendToDo':
        case 'ReportProgress':
            $('#showStatusUpdate').show();
            $('#showStatusIcon').hide();
            break;
        default:
            $('#showStatusUpdate').hide();
            $('#showStatusIcon').show();
        }
    }
    $('#current-action').text(label+':');

    //Set auto text in description field
    var descf = $('.ck-cd-comment-field textarea');
    if ($.trim(descf.val()) == currentActionAutoText) {
        descf.val(autotext);
        currentActionAutoText = autotext;
    } else {
        currentActionAutoText = '';
    }
}

function actionSubmit(e) {
    //var form = $(this);
    var action = $('#commitment_action input:checked').val();
    if ('Accept'==action) {
        var dueDate = $('#commitment_newDueDate').datepicker('getDate');
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        if (dueDate < today) {
            if (!confirm(Translator.trans('onsubmit.dueDateInPast'))) {
                e.preventDefault();
            }
        }
    }
}

$(document).ready(function(){
  $('form#c_detail').submit(actionSubmit);
});
