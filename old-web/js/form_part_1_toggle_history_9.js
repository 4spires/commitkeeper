jQuery(document).ready(function() {
    jQuery(".ck-cd-history-item").click(function() {
        $('.ck-cd-history-item-desc-short', this).toggle();
        $('.ck-cd-history-item-desc-long', this).toggle();
    });

    jQuery("#ck_history_sort").click(function() {
        var hlist = document.getElementById('history_list');
        var divs = hlist.children;
        for (var i = divs.length - 2; i >= 0; i--) {
            hlist.appendChild(divs[i]);
        }
        var newSortLabel = Translator.trans('history.sort.descending');
        var sortAsc = Translator.trans('history.sort.ascending');
        var sortLabel = $(this).text();
        if (sortLabel != sortAsc) {
            newSortLabel = sortAsc;
        }
        $(this).text(newSortLabel);
    });

    jQuery("#ck_history_views").click(function() {
        var NewViewLabel = Translator.trans('history.view.toFull');
        var toSummary = Translator.trans('history.view.toSummary');

        if ($(this).text() != toSummary) {
            // Show full history
            $('.ck-cd-history-item-desc-short').hide();
            $('.ck-cd-history-item-desc-long').show();

            NewViewLabel = toSummary;
        } else {
            // Just summary
            $('.ck-cd-history-item-desc-short').show();
            $('.ck-cd-history-item-desc-long').hide();
        }

        $(this).text(NewViewLabel);
    });
});
