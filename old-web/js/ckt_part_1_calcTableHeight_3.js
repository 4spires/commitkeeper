jQuery(document).ready(function() {
    // calculate the table height based on row count. min:100 max:467
    var ck_tableRows = jQuery(".scrollContent tr").length;
    var ck_tableHeight = jQuery(".scrollContent tr").height();
    var headerHeight = jQuery(".fixedHeader tr").height();
    var ck_heightNeeded = ck_tableRows * ck_tableHeight + headerHeight;
    var ck_tableHeightNew = 467;

    if (ck_heightNeeded < 100) {
        ck_tableHeightNew = 100;
    } else if (ck_heightNeeded > 467) {
        ck_tableHeightNew = 467;
    } else {
        ck_tableHeightNew = ck_heightNeeded;
    }

    jQuery(".scrollContent").height(ck_tableHeightNew);
});
