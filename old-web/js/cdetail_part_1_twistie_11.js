// handles the show/hide of attachments
jQuery(document).ready(function() {
    jQuery('.ck-twistie').bind('click', function() {
        var theDisplay = "";
        var theTarget = "";
        var ckTwistie = "";
        var whatID = jQuery(this).attr("id");
        if (whatID == "ck-twistie_supporting") {
            theTarget = ".bottom-content"
        } else if (whatID == "ck-twistie_attachments") {
            theTarget = ".ck-attachment_div_tb"
        }

        theDisplay = jQuery(theTarget).css("display");
        ckTwistie = (theDisplay == "none") ? "left bottom" : "left top";

        jQuery(this).css("background-position", ckTwistie);
        jQuery(theTarget).fadeToggle("slow");
    });
});
