(function (Translator) {
    // en
    Translator.add("onsubmit.dueDateInPast", "You are agreeing to a due date in the past. Are you sure?", "javascript", "en");
    Translator.add("error.contactNotSaved", "Error! Could not save new contact", "javascript", "en");
    Translator.add("janrain.actionText", "Or...sign in using your account with", "javascript", "en");
    Translator.add("tag.delete", "Delete", "javascript", "en");
    Translator.add("tag.add", "Add", "javascript", "en");
    Translator.add("history.sort.ascending", "Sort Ascending", "javascript", "en");
    Translator.add("history.sort.descending", "Sort Descending", "javascript", "en");
    Translator.add("history.view.toFull", "Switch to Full View", "javascript", "en");
    Translator.add("history.view.toSummary", "Switch to Summary View", "javascript", "en");
})(Translator);
