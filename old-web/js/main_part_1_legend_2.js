jQuery(document).ready(function() {
    if (jQuery('.ck-new-req-title').length == 0) {
        // Not on New Request, New Offer or New To Do
        jQuery("#ck_legend").css("display", "block") ;
        jQuery("#ck_copyright").css("margin-top", "25px");
    } else {
        // hide the legend for New and To Do
        jQuery("#ck_legend").css("display", "none") ;
        jQuery("#ck_copyright").css("margin-top", "0px");
    }
});
