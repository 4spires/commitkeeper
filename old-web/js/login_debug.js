/*!
 * William DURAND <william.durand1@gmail.com>
 * MIT Licensed
 */
;var Translator=(function(i,d){var e={},l=[],h=new RegExp(/^\w+\: +(.+)$/),f=new RegExp(/^\s*((\{\s*(\-?\d+[\s*,\s*\-?\d+]*)\s*\})|([\[\]])\s*(-Inf|\-?\d+)\s*,\s*(\+?Inf|\-?\d+)\s*([\[\]]))\s?(.+?)$/),o=new RegExp(/^\s*(\{\s*(\-?\d+[\s*,\s*\-?\d+]*)\s*\})|([\[\]])\s*(-Inf|\-?\d+)\s*,\s*(\+?Inf|\-?\d+)\s*([\[\]])/);function j(s,r){var t,p=Translator.placeHolderPrefix,q=Translator.placeHolderSuffix;for(t in r){var u=new RegExp(p+t+q,"g");if(u.test(s)){s=s.replace(u,r[t])}}return s}function g(r,t,z,p,v){var s=z||p||v,A=t;if(d==e[s]){if(d==e[v]){return r}s=v}if(d===A||null===A){for(var u=0;u<l.length;u++){if(c(s,l[u],r)||c(v,l[u],r)){A=l[u];break}}}if(c(s,A,r)){return e[s][A][r]}var y,w,x,q;while(s.length>2){y=s.length;w=s.split(/[\s_]+/);x=w[w.length-1];q=x.length;if(1===w.length){break}s=s.substring(0,y-(q+1));if(c(s,A,r)){return e[s][A][r]}}if(c(v,A,r)){return e[v][A][r]}return r}function c(p,q,r){if(d==e[p]){return false}if(d==e[p][q]){return false}if(d==e[p][q][r]){return false}return true}function m(C,s,z){var p,x,v=[],B=[],w=C.split(Translator.pluralSeparator),u=[];for(p=0;p<w.length;p++){var A=w[p];if(f.test(A)){u=A.match(f);v[u[0]]=u[u.length-1]}else{if(h.test(A)){u=A.match(h);B.push(u[1])}else{B.push(A)}}}for(x in v){if(o.test(x)){u=x.match(o);if(u[1]){var t=u[2].split(","),q;for(q in t){if(s==t[q]){return v[x]}}}else{var r=n(u[4]);var y=n(u[5]);if(("["===u[3]?s>=r:s>r)&&("]"===u[6]?s<=y:s<y)){return v[x]}}}}return B[b(s,z)]||B[0]||d}function n(p){if("-Inf"===p){return Number.NEGATIVE_INFINITY}else{if("+Inf"===p||"Inf"===p){return Number.POSITIVE_INFINITY}}return parseInt(p,10)}function b(r,p){var q=p;if("pt_BR"===q){q="xbr"}if(q.length>3){q=q.split("_")[0]}switch(q){case"bo":case"dz":case"id":case"ja":case"jv":case"ka":case"km":case"kn":case"ko":case"ms":case"th":case"tr":case"vi":case"zh":return 0;case"af":case"az":case"bn":case"bg":case"ca":case"da":case"de":case"el":case"en":case"eo":case"es":case"et":case"eu":case"fa":case"fi":case"fo":case"fur":case"fy":case"gl":case"gu":case"ha":case"he":case"hu":case"is":case"it":case"ku":case"lb":case"ml":case"mn":case"mr":case"nah":case"nb":case"ne":case"nl":case"nn":case"no":case"om":case"or":case"pa":case"pap":case"ps":case"pt":case"so":case"sq":case"sv":case"sw":case"ta":case"te":case"tk":case"ur":case"zu":return(r==1)?0:1;case"am":case"bh":case"fil":case"fr":case"gun":case"hi":case"ln":case"mg":case"nso":case"xbr":case"ti":case"wa":return((r===0)||(r==1))?0:1;case"be":case"bs":case"hr":case"ru":case"sr":case"uk":return((r%10==1)&&(r%100!=11))?0:(((r%10>=2)&&(r%10<=4)&&((r%100<10)||(r%100>=20)))?1:2);case"cs":case"sk":return(r==1)?0:(((r>=2)&&(r<=4))?1:2);case"ga":return(r==1)?0:((r==2)?1:2);case"lt":return((r%10==1)&&(r%100!=11))?0:(((r%10>=2)&&((r%100<10)||(r%100>=20)))?1:2);case"sl":return(r%100==1)?0:((r%100==2)?1:(((r%100==3)||(r%100==4))?2:3));case"mk":return(r%10==1)?0:1;case"mt":return(r==1)?0:(((r===0)||((r%100>1)&&(r%100<11)))?1:(((r%100>10)&&(r%100<20))?2:3));case"lv":return(r===0)?0:(((r%10==1)&&(r%100!=11))?1:2);case"pl":return(r==1)?0:(((r%10>=2)&&(r%10<=4)&&((r%100<12)||(r%100>14)))?1:2);case"cy":return(r==1)?0:((r==2)?1:(((r==8)||(r==11))?2:3));case"ro":return(r==1)?0:(((r===0)||((r%100>0)&&(r%100<20)))?1:2);case"ar":return(r===0)?0:((r==1)?1:((r==2)?2:(((r>=3)&&(r<=10))?3:(((r>=11)&&(r<=99))?4:5))));default:return 0}}function k(r,q){for(var p=0;p<r.length;p++){if(q===r[p]){return true}}return false}function a(){return i.documentElement.lang.replace("-","_")}return{locale:a(),fallback:"en",placeHolderPrefix:"%",placeHolderSuffix:"%",defaultDomain:"messages",pluralSeparator:"|",add:function(u,s,t,q){var r=q||this.locale||this.fallback,p=t||this.defaultDomain;if(!e[r]){e[r]={}}if(!e[r][p]){e[r][p]={}}e[r][p][u]=s;if(false===k(l,p)){l.push(p)}return this},trans:function(t,r,s,p){var q=g(t,s,p,this.locale,this.fallback);return j(q,r||{})},transChoice:function(v,s,r,t,p){var q=g(v,t,p,this.locale,this.fallback);var u=parseInt(s,10);if(d!=q&&!isNaN(u)){q=m(q,u,p||this.locale||this.fallback)}return j(q,r||{})},fromJSON:function(r){if(typeof r==="string"){r=JSON.parse(r)}if(r.locale){this.locale=r.locale}if(r.fallback){this.fallback=r.fallback}if(r.defaultDomain){this.defaultDomain=r.defaultDomain}if(r.translations){for(var p in r.translations){for(var q in r.translations[p]){for(var s in r.translations[p][q]){this.add(s,r.translations[p][q][s],q,p)}}}}return this},reset:function(){e={};l=[];this.locale=a()}}})(document,undefined);if(typeof window.define==="function"&&window.define.amd){window.define("Translator",[],function(){return Translator})};
(function (Translator) {
    Translator.fallback      = 'en';
    Translator.defaultDomain = 'javascript';
})(Translator);

(function (Translator) {
    // en
    Translator.add("onsubmit.dueDateInPast", "You are agreeing to a due date in the past. Are you sure?", "javascript", "en");
    Translator.add("error.contactNotSaved", "Error! Could not save new contact", "javascript", "en");
    Translator.add("janrain.actionText", "Or...sign in using your account with", "javascript", "en");
    Translator.add("tag.delete", "Delete", "javascript", "en");
    Translator.add("tag.add", "Add", "javascript", "en");
    Translator.add("history.sort.ascending", "Sort Ascending", "javascript", "en");
    Translator.add("history.sort.descending", "Sort Descending", "javascript", "en");
    Translator.add("history.view.toFull", "Switch to Full View", "javascript", "en");
    Translator.add("history.view.toSummary", "Switch to Summary View", "javascript", "en");
})(Translator);

function setupJainRainLogin(login_check_url) {
    if (typeof window.janrain !== 'object') window.janrain = {};
    if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};
    janrain_appliaction_url = 'https://commitkeeper.rpxnow.com';
    janrain.settings.tokenUrl = login_check_url;

    function isReady() { janrain.ready = true; };
    if (document.addEventListener) {
      document.addEventListener("DOMContentLoaded", isReady, false);
    } else {
      window.attachEvent('onload', isReady);
    }

    // Janinrain default setup
    if (typeof window.janrain.engage !== "object") window.janrain.engage = {};
    if (!janrain.settings.capture) janrain.settings.capture = {};
    if (!janrain.settings.common) janrain.settings.common = {};
    if (!janrain.settings.language) janrain.settings.language = 'en';
    if (!janrain.settings.packages) {
        janrain.settings.packages = ['login'];
    } else {
        if (janrain.settings.tokenUrl) janrain.settings.packages.push('login');
    }
    if (!janrain.settings.publish) janrain.settings.publish = {};
    if (!janrain.settings.share) janrain.settings.share = {};
    if (!janrain.settings.simpleshare) janrain.settings.simpleshare = {};
    if (!janrain.loadedPackages) janrain.loadedPackages = [];
    if (!janrain.settings.linkClass) janrain.settings.linkClass = 'janrainEngage';

    if (typeof janrain.settings.common.appUrl === 'undefined')janrain.settings.common.appUrl = janrain_appliaction_url;
    if (typeof janrain.settings.appUrl === 'undefined')janrain.settings.appUrl = janrain_appliaction_url;
    if (typeof janrain.settings.share.appUrl === 'undefined')janrain.settings.share.appUrl = janrain_appliaction_url;

    if (typeof janrain.settings.showAttribution === 'undefined')janrain.settings.showAttribution = true;
    if (typeof janrain.settings.type === 'undefined')janrain.settings.type = 'embed';
    if (typeof janrain.settings.format === 'undefined')janrain.settings.format = 'two column';
    if (typeof janrain.settings.width === 'undefined')janrain.settings.width = '362';
    if (typeof janrain.settings.providersPerPage === 'undefined')janrain.settings.providersPerPage = '6';
    if (typeof janrain.settings.actionText === 'undefined')janrain.settings.actionText = Translator.trans('janrain.actionText');
    if (typeof janrain.settings.fontColor === 'undefined')janrain.settings.fontColor = '#000';
    if (!janrain.settings.fontSize) janrain.settings.fontSize = '16px';
    if (typeof janrain.settings.fontFamily === 'undefined')janrain.settings.fontFamily = 'Arial, Helvetica, sans-serif';
    if (typeof janrain.settings.backgroundColor === 'undefined')janrain.settings.backgroundColor = '#ffffff';
    if (typeof janrain.settings.buttonBorderColor === 'undefined')janrain.settings.buttonBorderColor = '#CCCCCC';
    if (typeof janrain.settings.buttonBorderRadius === 'undefined')janrain.settings.buttonBorderRadius = '5';
    if (typeof janrain.settings.buttonBackgroundStyle === 'undefined')janrain.settings.buttonBackgroundStyle = 'gradient';
    if (typeof janrain.settings.borderWidth === 'undefined')janrain.settings.borderWidth = '15';
    if (typeof janrain.settings.borderColor === 'undefined')janrain.settings.borderColor = '#C0C0C0';
    if (typeof janrain.settings.borderRadius === 'undefined')janrain.settings.borderRadius = '10';
    if (typeof janrain.settings.appId === 'undefined')janrain.settings.appId = 'pmingiiojoaeogembina';
    janrain.settings.permissions = ["customizable_auth_widget_styling"];
    if (typeof janrain.settings.providers === 'undefined')janrain.settings.providers = [
        'google',
        'yahoo',
        'openid',
        'myopenid',
        'blogger',
        'flickr'];
    if (typeof janrain.settings.noReturnExperience === 'undefined')janrain.settings.noReturnExperience = false;
    if (typeof janrain.settings.maxProviders === 'undefined')janrain.settings.maxProviders = '6';
        if (typeof janrain.settings.share.attributionDisplay === 'undefined')janrain.settings.share.attributionDisplay = true;
    if (typeof janrain.settings.share.elementColor === 'undefined')janrain.settings.share.elementColor = '#333333';
    if (typeof janrain.settings.share.elementHoverBackgroundColor === 'undefined')janrain.settings.share.elementHoverBackgroundColor = '#eeeeee';
    if (typeof janrain.settings.share.elementButtonBorderRadius === 'undefined')janrain.settings.share.elementButtonBorderRadius = '6';
    if (typeof janrain.settings.share.elementBorderColor === 'undefined')janrain.settings.share.elementBorderColor = '#cccccc';
    if (typeof janrain.settings.share.elementBackgroundColor === 'undefined')janrain.settings.share.elementBackgroundColor = '#f6f6f6';
    if (typeof janrain.settings.share.elementLinkColor === 'undefined')janrain.settings.share.elementLinkColor = '#009DDC';
    if (typeof janrain.settings.share.elementBorderRadius === 'undefined')janrain.settings.share.elementBorderRadius = '3';
    if (typeof janrain.settings.share.elementButtonBoxShadow === 'undefined')janrain.settings.share.elementButtonBoxShadow = '3';
    if (typeof janrain.settings.share.modalOpacity === 'undefined')janrain.settings.share.modalOpacity = '0.5';
    if (typeof janrain.settings.share.modalBorderRadius === 'undefined')janrain.settings.share.modalBorderRadius = '5';
    if (typeof janrain.settings.share.bodyColor === 'undefined')janrain.settings.share.bodyColor = '#333333';
    if (typeof janrain.settings.share.bodyTabBackgroundColor === 'undefined')janrain.settings.share.bodyTabBackgroundColor = '#f8f8f8';
    if (typeof janrain.settings.share.bodyTabColor === 'undefined')janrain.settings.share.bodyTabColor = '#000000';
    if (typeof janrain.settings.share.bodyContentBackgroundColor === 'undefined')janrain.settings.share.bodyContentBackgroundColor = '#ffffff';
    if (typeof janrain.settings.share.bodyBackgroundColorOverride === 'undefined')janrain.settings.share.bodyBackgroundColorOverride = false;
    if (typeof janrain.settings.share.bodyFontFamily === 'undefined')janrain.settings.share.bodyFontFamily = 'Helvetica';
    if (typeof janrain.settings.share.bodyBackgroundColor === 'undefined')janrain.settings.share.bodyBackgroundColor = '#009DDC';
    if (typeof janrain.settings.share.modalBackgroundColor === 'undefined')janrain.settings.share.modalBackgroundColor = '#000000';
    janrain.settings.share.permissions = [];
    if (typeof janrain.settings.share.providers === 'undefined')janrain.settings.share.providers = [];
    if (typeof janrain.settings.share.providersEmail === 'undefined')janrain.settings.share.providersEmail = [];
    if (typeof janrain.settings.share.modes === 'undefined')janrain.settings.share.modes = ["broadcast"];

    /*
*  _scriptLoader
*
*  Loads script dynamically and allows for callbacks and a timeout.
*
*  @return {Object} Public methods for _loadDyanmicScript
*  @private
*/
function _scriptLoader(src, callback) {
    var _callback = callback,
        _timeout = 200,
        _useTimeout = false,
        _timeoutCallback,
        _pollCount = 0,
        _pollTimeout,
        _script = document.createElement('script'),
        _firstScript = document.getElementsByTagName('script')[0],
        _finished = false;

    _script.src = src;
    _script.setAttribute('type', 'text/javascript');

    _script.onload = _script.onerror = _script.onreadystatechange = function(event) {
        if (!_finished && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
            _finish(event);
        }
    }

    function _load() {
        _firstScript.parentNode.insertBefore(_script, _firstScript);
        if (_useTimeout) _pollLoad();
    }

    function _finish(event) {
        _finished = true;
        if (typeof _pollTimeout !== 'undefined') {
            clearTimeout(_pollTimeout);
        }
        // event is a string when loading a script fails for any reason.
        if (typeof event === 'string') {
            if (typeof _timeoutCallback === 'function') _timeoutCallback(event);
            return true;
        }
        if (typeof event === 'object' || typeof event === 'undefined') {
            if (typeof event === 'object' && event.type === 'error') {
                if (typeof _timeoutCallback === 'function') _timeoutCallback(event);
            } else {
                if (typeof _callback === 'function') _callback();
            }
            return true;
        }
    }

    function _pollLoad() {
        _pollCount++;
        if (_finished) return true;
        if (_pollCount < _timeout) {
            _pollTimeout = setTimeout(_pollLoad, 50);
        } else {
            _finish("Load Timeout Error");
        }
    }

    return {
        setTimeoutCallback: function(callback) {
            _useTimeout = true;
            _timeoutCallback = callback;
            return this;
        },
        setCallback: function(callback) {
            _callback = callback;
            return this;
        },
        setTimeoutLimit: function(time) {
            _timeout = time;
            return this;
        },
        load: function() {
            _load();
        }
    }
}

function _loadDynamicScript(src, callback) {
    _scriptLoader(src, callback).load();
}

    function getPackagePath(packages) {
        var rootPath = document.location.protocol === 'https:' ? "https://d29usylhdk1xyu.cloudfront.net/" : "http://widget-cdn.rpxnow.com/";
        var path = rootPath + 'manifest/' + packages.join(':') + '?version=' + encodeURIComponent('2013.13_ws_hotfix1_widgets_rc2');
        return path;
    }
    function getTranslationPath(language, widget) {
        var rootPath = document.location.protocol === 'https:' ? "https://d29usylhdk1xyu.cloudfront.net/" : "http://widget-cdn.rpxnow.com/";
        var path = rootPath + 'translations/' + widget + '/' + language;
        return path;
    }
    function loadPackages(loaded, packages) {
        if (packages.length === 0) return false;
        if (loaded === packages.length) {
            var widgetPath = getPackagePath(packages);
            _loadDynamicScript(widgetPath);
        } else {
            if (!inArray(janrain.loadedPackages, packages[loaded])) {
                janrain.loadedPackages.push(packages[loaded]);
                if ((packages[loaded] === "login"&& janrain.settings.language === "en")
                    || packages[loaded] === "capture"
                    || packages[loaded] === "simpleshare") {
                    loadPackages(loaded + 1, packages);
                } else {
                    _loadDynamicScript(getTranslationPath(janrain.settings.language, packages[loaded]), function() {
                        loadPackages(loaded + 1, packages);
                    });
                }
            } else {
                deleteItemFromArray(packages, loaded);
                loadPackages(loaded, packages);
            }
        }
    }
    function arrayToObject(array) {
        var uniqueObject = {};
        for (var i = 0, l = array.length; i < l; i++) {
            uniqueObject[array[i]] = array[i];
        }
        return uniqueObject;
    }
    function deleteItemFromArray(array, from, to) {
        var rest = array.slice((to || from) + 1 || array.length);
        array.length = from < 0 ? array.length + from : from;
        return array.push.apply(array, rest);
    }
    function inArray(array, item) {
        var arrayObject = arrayToObject(array);
        return arrayObject.hasOwnProperty(item);
    }
    function unique(array){
        var uniqueObject = arrayToObject(array);
        var unique = [];
        for (var key in uniqueObject){
            if (uniqueObject.hasOwnProperty(key)) unique.push(key);
        }
        return unique;
    }

    janrain.settings.packages = unique(janrain.settings.packages);
    janrain.settings.packages.sort();
    loadPackages(0, janrain.settings.packages);
}

function on_userselect(email) {
    var form = document.forms[0];
    form.elements['_username'].value = email;
    form.elements['_password'].value = 'e';
    if (email!='') {
        form.submit();
    }
}

function show_usernames() {
    var usernames = document.getElementById('userselect');
    usernames.style.display='block';
    usernames.focus();
    usernames.click();
}
