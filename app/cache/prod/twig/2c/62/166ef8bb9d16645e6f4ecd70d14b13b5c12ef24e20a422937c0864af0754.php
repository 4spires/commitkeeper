<?php

/* CKBundle:Main:maintabs.html.twig */
class __TwigTemplate_2c62166ef8bb9d16645e6f4ecd70d14b13b5c12ef24e20a422937c0864af0754 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"ck-toptab-area\">
        <ul>
";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tabs"]) ? $context["tabs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 4
            echo "<li class=\"ck-toptab ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tab"], "cssClass", array()), "html", null, true);
            echo "\">
        <div class=\"ck-toptab-left\"></div>
        <div class=\"ck-toptab-name\"><a id=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($context["tab"], "id", array()), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env,             // line 7
(isset($context["TableSortUrl"]) ? $context["TableSortUrl"] : null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tab"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["tab"], "id", array())), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($context["tab"], "count", array()), "html", null, true);
            echo ")</a>
        </div>
        <div class=\"ck-toptab-right\"></div>
</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "        </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "CKBundle:Main:maintabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 12,  36 => 7,  33 => 6,  27 => 4,  23 => 3,  19 => 1,);
    }
}
