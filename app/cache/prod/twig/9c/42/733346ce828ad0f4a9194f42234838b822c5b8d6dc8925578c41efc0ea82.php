<?php

/* CKBundle:Security:login.html.twig */
class __TwigTemplate_9c42733346ce828ad0f4a9194f42234838b822c5b8d6dc8925578c41efc0ea82 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("::base.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'debug' => array($this, 'block_debug'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        $this->env->loadTemplate("CKBundle:Security:cssdef_login.html.twig")->display($context);
    }

    // line 8
    public function block_javascripts($context, array $blocks = array())
    {
        // line 9
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7152814_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7152814_0") : $this->env->getExtension('assets')->getAssetUrl("js/login_translator.min_1.js");
            // line 16
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "7152814_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7152814_1") : $this->env->getExtension('assets')->getAssetUrl("js/login_config_2.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "7152814_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7152814_2") : $this->env->getExtension('assets')->getAssetUrl("js/login_part_3_en_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
            // asset "7152814_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7152814_3") : $this->env->getExtension('assets')->getAssetUrl("js/login_part_4_jainrain_1.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        } else {
            // asset "7152814"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7152814") : $this->env->getExtension('assets')->getAssetUrl("js/login.js");
            echo "<script language=\"javascript\" type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
";
        }
        unset($context["asset_url"]);
        // line 18
        echo "<script language=\"javascript\" type=\"text/javascript\"><!--
setupJainRainLogin('";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["login_check_url"]) ? $context["login_check_url"] : null), "html", null, true);
        echo "');
//--></script>
";
    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        // line 25
        echo "<div id=\"wrapper\">
  <img id=\"commitkeeper\" src=\"/images/login_banner2.png\" alt=\"";
        // line 26
        echo $this->env->getExtension('translator')->getTranslator()->trans("CommitKeeper", array(), "messages");
        echo "\" />

";
        // line 28
        if ((twig_length_filter($this->env, (isset($context["openIds"]) ? $context["openIds"] : null)) > 0)) {
            // line 29
            echo "  <div id=\"add_provider\" class=\"whitebox\">
    ";
            // line 30
            echo $this->env->getExtension('translator')->getTranslator()->transChoice("login.addOpenId", twig_length_filter($this->env, (isset($context["openIds"]) ? $context["openIds"] : null)), array(), "messages");
            // line 31
            echo "    <ul>
";
            // line 32
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["openIds"]) ? $context["openIds"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["oid"]) {
                // line 33
                echo "      <li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["oid"], "provider", array()), "html", null, true);
                echo "</li>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oid'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "    </ul>
    <a href=\"";
            // line 36
            echo $this->env->getExtension('routing')->getPath("clean_login");
            echo "\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.cancelAddOpenId", array(), "messages");
            echo "</a>
  </div>
";
        }
        // line 39
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 40
            echo "  <div class=\"error whitebox\">";
            echo (($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message", array())) ? ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message", array()), array(), "validators")) : ($this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageData", array()), "validators")));
            echo "</div>
";
        }
        // line 42
        echo "
  <div id=\"login\" class=\"whitebox\">

<form action=\"";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["login_check_url"]) ? $context["login_check_url"] : null), "html", null, true);
        echo "\" method=\"post\">
<table class=\"pwdlogin\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
  <tr>
    <td class=\"first\"><label for=\"username\">";
        // line 48
        echo $this->env->getExtension('translator')->getTranslator()->trans("user.email.short", array(), "messages");
        echo ":</label></td>
";
        // line 49
        if ((isset($context["isRememberMe"]) ? $context["isRememberMe"] : null)) {
            // line 50
            echo "    <td>
      <span class=\"username\">";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["default_username"]) ? $context["default_username"] : null), "html", null, true);
            echo "</span>
      <input type=\"hidden\" id=\"username\" name=\"_username\" value=\"";
            // line 52
            echo twig_escape_filter($this->env, (isset($context["default_username"]) ? $context["default_username"] : null), "html", null, true);
            echo "\"/>
    </td>
    <td></td>
";
        } else {
            // line 56
            echo "    <td>
      <input type=\"text\" id=\"username\" name=\"_username\" maxlength=\"200\" value=\"";
            // line 57
            echo twig_escape_filter($this->env, (isset($context["default_username"]) ? $context["default_username"] : null), "html", null, true);
            echo "\" onfocus=\"if(this.value==this.defaultValue){var self=this;setTimeout(function() {self.select();}, 0);}\" />
    </td>";
            // line 58
            $this->displayBlock('debug', $context, $blocks);
        }
        // line 60
        echo "  </tr>
  <tr>
    <td class=\"first\"><label for=\"password\">";
        // line 62
        echo $this->env->getExtension('translator')->getTranslator()->trans("password.password", array(), "messages");
        echo ":</label></td>
    <td><input type=\"password\" id=\"password\" name=\"_password\" /></td>
    <td></td>
  </tr>
  <tr>
    <td class=\"first\"></td>
    <td>
      <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" checked=\"checked\"/>
      <label for=\"remember_me\">";
        // line 70
        echo $this->env->getExtension('translator')->getTranslator()->trans("login.rememberMe", array(), "messages");
        echo "</label>
    </td>
    <td></td>
  </tr>
  <tr>
    <td colspan=\"2\" align=\"center\">
      <input class=\"login_submit\" type=\"submit\" value=\"";
        // line 77
        if ((isset($context["isRememberMe"]) ? $context["isRememberMe"] : null)) {
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.button.verify", array(), "messages");
        } else {
            // line 78
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.button.normal", array(), "messages");
        }
        // line 79
        echo "\" />
";
        // line 80
        if ((isset($context["isRememberMe"]) ? $context["isRememberMe"] : null)) {
            // line 81
            echo "    <a class=\"log_out buttonlink\" href=\"";
            echo $this->env->getExtension('routing')->getPath("logout");
            echo "\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.logout", array(), "messages");
            echo "</a>
";
        }
        // line 83
        echo "    </td>
    <td></td>
  </tr>
";
        // line 86
        if ( !((isset($context["isRememberMe"]) ? $context["isRememberMe"] : null) || (twig_length_filter($this->env, (isset($context["openIds"]) ? $context["openIds"] : null)) > 0))) {
            // line 87
            echo "  <tr>
    <td colspan=\"2\" align=\"center\">
      ";
            // line 89
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.noAccount", array(), "messages");
            // line 90
            echo "      <a href=\"";
            echo $this->env->getExtension('routing')->getPath("CK_register");
            echo "\">";
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.createOne", array(), "messages");
            echo "</a>
      <input class=\"forgot_password\" type=\"submit\" name=\"forgotPW\" value=\"";
            // line 91
            echo $this->env->getExtension('translator')->getTranslator()->trans("login.button.forgot", array(), "messages");
            echo "\" />
    </td>
    <td></td>
  </tr>
";
        }
        // line 96
        echo "</table>
<input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 97
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />
</form>

  </div>

  <div id=\"janrainEngageEmbed\"></div>

</div>
";
    }

    // line 58
    public function block_debug($context, array $blocks = array())
    {
        echo "<td></td>";
    }

    public function getTemplateName()
    {
        return "CKBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 58,  264 => 97,  261 => 96,  253 => 91,  246 => 90,  244 => 89,  240 => 87,  238 => 86,  233 => 83,  225 => 81,  223 => 80,  220 => 79,  217 => 78,  213 => 77,  204 => 70,  193 => 62,  189 => 60,  186 => 58,  182 => 57,  179 => 56,  172 => 52,  168 => 51,  165 => 50,  163 => 49,  159 => 48,  153 => 45,  148 => 42,  142 => 40,  140 => 39,  132 => 36,  129 => 35,  120 => 33,  116 => 32,  113 => 31,  111 => 30,  108 => 29,  106 => 28,  101 => 26,  98 => 25,  95 => 24,  88 => 19,  85 => 18,  53 => 16,  49 => 9,  46 => 8,  42 => 4,  39 => 3,  11 => 1,);
    }
}
