<?php

/* ::base.html.twig */
class __TwigTemplate_bd5c13ccaa5e1c368d1ff1d4ebbae07a5dab45450b5e6c667d3dcfcd60c832f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head_extra' => array($this, 'block_head_extra'),
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_javascripts' => array($this, 'block_main_javascripts'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'onload' => array($this, 'block_onload'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "locale", array()), "html", null, true);
        echo "\">
<head>
  <meta charset=\"UTF-8\" />
  ";
        // line 5
        $this->displayBlock('head_extra', $context, $blocks);
        // line 6
        echo "  <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
  <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
  ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "  ";
        $this->displayBlock('main_javascripts', $context, $blocks);
        // line 11
        echo "  ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "</head>
<body";
        // line 13
        if ((array_key_exists("HasOnload", $context) && (isset($context["HasOnload"]) ? $context["HasOnload"] : null))) {
            echo " onload=\"initpage();\"";
        }
        echo ">
";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        if ((array_key_exists("HasOnload", $context) && (isset($context["HasOnload"]) ? $context["HasOnload"] : null))) {
            // line 16
            echo "<script language=\"javascript\" type=\"text/javascript\"><!--
function initpage() {
";
            // line 18
            $this->displayBlock('onload', $context, $blocks);
            // line 19
            echo "}
//--></script>
";
        }
        // line 22
        echo "</body>
</html>
";
    }

    // line 5
    public function block_head_extra($context, array $blocks = array())
    {
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo $this->env->getExtension('translator')->getTranslator()->trans("CommitKeeper", array(), "messages");
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 10
    public function block_main_javascripts($context, array $blocks = array())
    {
    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
    }

    // line 18
    public function block_onload($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 18,  114 => 14,  109 => 11,  104 => 10,  99 => 9,  93 => 6,  88 => 5,  82 => 22,  77 => 19,  75 => 18,  71 => 16,  69 => 15,  67 => 14,  61 => 13,  58 => 12,  55 => 11,  52 => 10,  50 => 9,  46 => 8,  42 => 7,  37 => 6,  35 => 5,  29 => 2,  26 => 1,);
    }
}
