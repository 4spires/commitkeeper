<?php

/* CKBundle:Form:fields.html.twig */
class __TwigTemplate_d664917674df5af5bdc1b64aa15c0905924ded51d4fb6c843f362c3195557792 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_row' => array($this, 'block_field_row'),
            'info_label' => array($this, 'block_info_label'),
            'info_widget' => array($this, 'block_info_widget'),
            'gravatar_default_widget' => array($this, 'block_gravatar_default_widget'),
            'attachment_widget' => array($this, 'block_attachment_widget'),
            'statusUpdate_widget' => array($this, 'block_statusUpdate_widget'),
            'form_errors' => array($this, 'block_form_errors'),
            'collection_errors' => array($this, 'block_collection_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('field_row', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('info_label', $context, $blocks);
        // line 15
        echo "
";
        // line 16
        $this->displayBlock('info_widget', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('gravatar_default_widget', $context, $blocks);
        // line 35
        echo "
";
        // line 36
        $this->displayBlock('attachment_widget', $context, $blocks);
        // line 48
        echo "
";
        // line 49
        $this->displayBlock('statusUpdate_widget', $context, $blocks);
        // line 56
        echo "
";
        // line 58
        echo "
";
        // line 59
        $this->displayBlock('form_errors', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('collection_errors', $context, $blocks);
    }

    // line 2
    public function block_field_row($context, array $blocks = array())
    {
        // line 3
        ob_start();
        // line 4
        echo "    <div";
        if (((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) ? $context["class"] : null), false)) : (false))) {
            echo " class=\"";
            echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : null), "html", null, true);
            echo "\"";
        }
        echo ">
        ";
        // line 5
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'label', (twig_test_empty($_label_ = ((array_key_exists("label", $context)) ? (_twig_default_filter((isset($context["label"]) ? $context["label"] : null), null)) : (null))) ? array() : array("label" => $_label_)));
        echo "
        ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget', array("attr" => ((array_key_exists("attr", $context)) ? (_twig_default_filter((isset($context["attr"]) ? $context["attr"] : null), null)) : (null))));
        echo "
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 12
    public function block_info_label($context, array $blocks = array())
    {
        // line 13
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["label"]) ? $context["label"] : null), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
        echo ": </span>
";
    }

    // line 16
    public function block_info_widget($context, array $blocks = array())
    {
        // line 17
        ob_start();
        // line 18
        echo "<span  id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\" ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\" ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
        echo "</span>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 24
    public function block_gravatar_default_widget($context, array $blocks = array())
    {
        // line 25
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 27
            echo "<tr>
 <td><img src=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "vars", array()), "url", array()), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["child"], "vars", array()), "value", array()), "html", null, true);
            echo "&amp;f=y\"/></td>
 <td>";
            // line 29
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'widget');
            echo "<br />
     ";
            // line 30
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($context["child"], 'label');
            echo "</td>
</tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "</table>
";
    }

    // line 36
    public function block_attachment_widget($context, array $blocks = array())
    {
        // line 37
        echo "<table class=\"attachment\"><tr>
<td>";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'row');
        echo "</td><td>";
        // line 39
        $context["link"] = ((array_key_exists("link", $context)) ? (_twig_default_filter((isset($context["link"]) ? $context["link"] : null), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "islink", array()), "vars", array()), "checked", array()))) : ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "islink", array()), "vars", array()), "checked", array())));
        echo         // line 40
$this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "islink", array()), 'widget', array("checked" => (isset($context["link"]) ? $context["link"] : null)));
        // line 41
        if ((isset($context["link"]) ? $context["link"] : null)) {
            // line 42
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "linkUrl", array()), 'row');
        } else {
            // line 44
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "file", array()), 'row');
        }
        // line 45
        echo "</td>
</tr></table>
";
    }

    // line 49
    public function block_statusUpdate_widget($context, array $blocks = array())
    {
        // line 50
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">
";
        // line 51
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["choices"]) ? $context["choices"] : null));
        foreach ($context['_seq'] as $context["optval"] => $context["choice"]) {
            // line 52
            echo "<option value=\"";
            echo twig_escape_filter($this->env, $context["optval"], "html", null, true);
            echo "\"";
            if (($context["optval"] == (isset($context["value"]) ? $context["value"] : null))) {
                echo " selected=\"selected\"";
            }
            echo " data-imagesrc=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($context["choice"], "img", array())), "html", null, true);
            echo "\" data-description=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($context["choice"], "text", array()), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : null)), "html", null, true);
            echo "\"></option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['optval'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "</select>
";
    }

    // line 59
    public function block_form_errors($context, array $blocks = array())
    {
        // line 60
        ob_start();
        // line 61
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
            // line 62
            echo "    <ul class=\"error\">
        ";
            // line 63
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 64
                echo "            <li>";
                echo twig_escape_filter($this->env, (((null === $this->getAttribute(                // line 65
$context["error"], "messagePluralization", array()))) ? ($this->env->getExtension('translator')->trans($this->getAttribute(                // line 66
$context["error"], "messageTemplate", array()), $this->getAttribute($context["error"], "messageParameters", array()), "validators")) : ($this->env->getExtension('translator')->transchoice($this->getAttribute(                // line 67
$context["error"], "messageTemplate", array()), $this->getAttribute($context["error"], "messagePluralization", array()), $this->getAttribute($context["error"], "messageParameters", array()), "validators"))), "html", null, true);
                // line 68
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "    </ul>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    // line 75
    public function block_collection_errors($context, array $blocks = array())
    {
        // line 76
        ob_start();
        // line 77
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : null)) > 0)) {
            // line 78
            echo "    <ul class=\"error\">
        ";
            // line 79
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 80
                echo "            <li>";
                echo $this->env->getExtension('translator')->trans($this->getAttribute($context["error"], "messageTemplate", array()), $this->getAttribute($context["error"], "messageParameters", array()), "validators");
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "    </ul>
    ";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "CKBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  296 => 82,  287 => 80,  283 => 79,  280 => 78,  277 => 77,  275 => 76,  272 => 75,  265 => 70,  258 => 68,  256 => 67,  255 => 66,  254 => 65,  252 => 64,  248 => 63,  245 => 62,  242 => 61,  240 => 60,  237 => 59,  232 => 54,  215 => 52,  211 => 51,  206 => 50,  203 => 49,  197 => 45,  194 => 44,  191 => 42,  189 => 41,  187 => 40,  185 => 39,  182 => 38,  179 => 37,  176 => 36,  171 => 33,  162 => 30,  158 => 29,  153 => 28,  150 => 27,  146 => 26,  141 => 25,  138 => 24,  130 => 20,  118 => 19,  114 => 18,  112 => 17,  109 => 16,  102 => 13,  99 => 12,  91 => 7,  87 => 6,  83 => 5,  74 => 4,  72 => 3,  69 => 2,  65 => 75,  62 => 74,  60 => 59,  57 => 58,  54 => 56,  52 => 49,  49 => 48,  47 => 36,  44 => 35,  42 => 24,  39 => 23,  37 => 16,  34 => 15,  32 => 12,  29 => 11,  27 => 2,);
    }
}
