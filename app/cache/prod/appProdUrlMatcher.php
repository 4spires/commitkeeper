<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // bazinga_jstranslation_js
        if (0 === strpos($pathinfo, '/translations') && preg_match('#^/translations(?:/(?P<domain>[\\w]+)(?:\\.(?P<_format>js|json))?)?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_bazinga_jstranslation_js;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'bazinga_jstranslation_js')), array (  '_controller' => 'bazinga.jstranslation.controller:getTranslationsAction',  'domain' => 'messages',  '_format' => 'js',));
        }
        not_bazinga_jstranslation_js:

        // login_check
        if (preg_match('#^/(?P<_locale>en|es)/login_check$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_check')), array ());
        }

        // logout
        if (preg_match('#^/(?P<_locale>en|es)/logout$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'logout')), array ());
        }

        // login
        if (preg_match('#^/(?P<_locale>en|es)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login')), array (  '_controller' => 'ck.security:loginAction',  'debug' => '1',));
        }

        // clean_login
        if (preg_match('#^/(?P<_locale>en|es)/login/clean$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'clean_login')), array (  '_controller' => 'ck.security:cleanLoginAction',));
        }

        // access_denied
        if (preg_match('#^/(?P<_locale>en|es)/access_denied$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'access_denied')), array (  '_controller' => 'ck.security:accessDeniedAction',));
        }

        // CK_root
        if (preg_match('#^/(?P<_locale>en|es)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'CK_root');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_root')), array (  '_controller' => 'ck.registration:registerAction',  'root' => true,  'userStr' => false,  'table' => 'table',  'reqSelect' => false,));
        }

        // CK_register
        if (preg_match('#^/(?P<_locale>en|es)/register$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_register')), array (  '_controller' => 'ck.registration:registerAction',  'root' => false,));
        }

        // CK_register_addauth
        if (preg_match('#^/(?P<_locale>en|es)/register/addauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_register_addauth')), array (  '_controller' => 'ck.registration:registerOrAddAuthAction',));
        }

        // CK_confirm
        if (preg_match('#^/(?P<_locale>en|es)/confirm$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_confirm')), array (  '_controller' => 'ck.registration:confirmAction',));
        }

        // CK_newopenid
        if (preg_match('#^/(?P<_locale>en|es)/setauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_newopenid')), array (  '_controller' => 'ck.registration:newOpenIdAction',));
        }

        // CK_addopenid
        if (preg_match('#^/(?P<_locale>en|es)/addauth$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_addopenid')), array (  '_controller' => 'ck.registration:addOpenIdAction',));
        }

        // CK_newContact
        if (preg_match('#^/(?P<_locale>en|es)/newcontact$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_newContact;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_newContact')), array (  '_controller' => 'ck.ajax:newContactAction',));
        }
        not_CK_newContact:

        // CK_upgrade
        if (preg_match('#^/(?P<_locale>en|es)/upgrade$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_upgrade')), array (  '_controller' => 'ck.upgrade:indexAction',  'action' => 'none',));
        }

        // CK_upgrade_action
        if (preg_match('#^/(?P<_locale>en|es)/upgrade/(?P<action>action\\w+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_upgrade_action')), array (  '_controller' => 'ck.upgrade:indexAction',));
        }

        // CK_default
        if (preg_match('#^/(?P<_locale>en|es)/table$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_default')), array (  '_controller' => 'ck.table:indexAction',  'userStr' => false,  'table' => 'table',  'reqSelect' => false,));
        }

        // CK_table
        if (preg_match('#^/(?P<_locale>en|es)/(?P<table>table(Sort\\w+)?)(?:/(?P<reqSelect>PendingByMe|PendingByOthers|AllActive|Closed))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_table')), array (  '_controller' => 'ck.table:indexAction',  'userStr' => false,  'reqSelect' => false,));
        }

        // CK_user_table
        if (preg_match('#^/(?P<_locale>en|es)/(?P<userStr>user\\d+)/(?P<table>table(Sort\\w+)?)(?:/(?P<reqSelect>PendingByMe|PendingByOthers|AllActive|Closed))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_user_table')), array (  '_controller' => 'ck.table:indexAction',  'reqSelect' => false,));
        }

        // CK_new_commitment
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_new_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_commitment')), array (  '_controller' => 'ck.commitment.new:newAction',  'superReqId' => false,));
        }
        not_CK_new_commitment:

        // CK_new_commitment_posted
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_new_commitment_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_commitment_posted')), array (  '_controller' => 'ck.commitment.new_posted:newAction',  'superReqId' => false,));
        }
        not_CK_new_commitment_posted:

        // CK_new_sub_commitment
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)/(?P<superReqId>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_new_sub_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_sub_commitment')), array (  '_controller' => 'ck.commitment.new:newAction',));
        }
        not_CK_new_sub_commitment:

        // CK_new_sub_commitment_posted
        if (preg_match('#^/(?P<_locale>en|es)/new/(?P<type>request|offer|toDo)/(?P<superReqId>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_new_sub_commitment_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_new_sub_commitment_posted')), array (  '_controller' => 'ck.commitment.new_posted:newAction',));
        }
        not_CK_new_sub_commitment_posted:

        // CK_commitment
        if (preg_match('#^/(?P<_locale>en|es)/commitment$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment')), array (  '_controller' => 'ck.commitment.show:indexAction',  'commitmentIdStr' => false,));
        }
        not_CK_commitment:

        // CK_commitment_user
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_user;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user')), array (  '_controller' => 'ck.commitment.show:indexAction',  'commitmentIdStr' => false,));
        }
        not_CK_commitment_user:

        // CK_commitment_user_id
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_user_id;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user_id')), array (  '_controller' => 'ck.commitment.show:indexAction',));
        }
        not_CK_commitment_user_id:

        // CK_commitment_user_id_posted
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<userStr>user\\d+)/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_commitment_user_id_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_user_id_posted')), array (  '_controller' => 'ck.commitment.show_posted:indexAction',));
        }
        not_CK_commitment_user_id_posted:

        // CK_commitment_id
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_CK_commitment_id;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_id')), array (  '_controller' => 'ck.commitment.show:indexAction',  'userStr' => false,));
        }
        not_CK_commitment_id:

        // CK_commitment_id_posted
        if (preg_match('#^/(?P<_locale>en|es)/commitment/(?P<commitmentIdStr>\\d+)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_CK_commitment_id_posted;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_commitment_id_posted')), array (  '_controller' => 'ck.commitment.show_posted:indexAction',  'userStr' => false,));
        }
        not_CK_commitment_id_posted:

        // CK_attachment
        if (preg_match('#^/(?P<_locale>en|es)/attachment/(?P<attId>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_attachment')), array (  '_controller' => 'ck.attachment:indexAction',));
        }

        // CK_picture
        if (preg_match('#^/(?P<_locale>en|es)/picture/(?P<pictureId>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_picture')), array (  '_controller' => 'ck.picture:indexAction',));
        }

        // CK_edit_settings
        if (preg_match('#^/(?P<_locale>en|es)/editSettings$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_settings')), array (  '_controller' => 'ck.settings:editSettingsAction',));
        }

        // CK_edit_id
        if (preg_match('#^/(?P<_locale>en|es)/editId$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_id')), array (  '_controller' => 'ck.settings:editIdAction',));
        }

        // CK_edit_picture
        if (preg_match('#^/(?P<_locale>en|es)/editPicture$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_picture')), array (  '_controller' => 'ck.settings:editPictureAction',));
        }

        // CK_edit_group
        if (preg_match('#^/(?P<_locale>en|es)/editGroup$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_group')), array (  '_controller' => 'ck.settings:editGroupAction',));
        }

        // CK_edit_group_categories
        if (preg_match('#^/(?P<_locale>en|es)/editGroupCategories$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_group_categories')), array (  '_controller' => 'ck.settings:editGroupCategoriesAction',));
        }

        // CK_edit_usertags
        if (preg_match('#^/(?P<_locale>en|es)/editUserTags$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_usertags')), array (  '_controller' => 'ck.settings:editPrefsAction',));
        }

        // CK_edit_usertags_category
        if (preg_match('#^/(?P<_locale>en|es)/editUserTags/(?P<category_id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'CK_edit_usertags_category')), array (  '_controller' => 'ck.settings:editUserTagCategoryAction',));
        }

        // access_denied_root
        if ($pathinfo === '/access_denied') {
            return array (  '_controller' => 'ck.security:accessDeniedAction',  '_route' => 'access_denied_root',);
        }

        // choose_language
        if (preg_match('#^/(?P<url>.*)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'choose_language')), array (  '_controller' => 'ck.settings:chooseLanguageAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
