<?php

namespace FSpires\CommitKeeperBundle\Controller;

/**
 * This code has been auto-generated by the JMSDiExtraBundle.
 *
 * Manual changes to it will be lost.
 */
class SettingsController__JMSInjector
{
    public static function inject($container) {
        $instance = new \FSpires\CommitKeeperBundle\Controller\SettingsController();
        return $instance;
    }
}
