<?php
namespace FSpires\CommitKeeperBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use FSpires\CommitKeeperBundle\Model\Enum\Selection as RS;

/**
 * Abstract controller for the main pages of the CKBundle
 * Used to contain common methods and data for the controllers
 * that will be using an User or UserId or the sidebar buttons
 */
class MainpageController extends AbstractController {
  private $userId  = false;
  private $user    = false;

  /**
   * Switch to another user by logging out and redirect to the login page
   *
   * @throws AccessDeniedException
   */
  protected function logoutToAnotherUser($thisUri)
  {
    $this->session->set('_security.secured_area.target_path', $thisUri);
    return $this->redirect('/logout', Response::HTTP_TEMPORARY_REDIRECT);
  }

  /**
   * Make a URL to go to a given table/list of commitments
   */
  protected function makeTableUrl($table, $reqSelect, $userStr = false)
  {
    $routeName = 'CK_table';
    $args = array('table'=>$table, 'reqSelect'=>$reqSelect);
    if ($userStr) {
      $routeName = 'CK_user_table';
      $args['userStr'] = $userStr;
    }
    return $this->router->generate($routeName, $args);
  }

  /**
   * This will initiate the current user id
   *
   * @param bool|string $userStr May contain "user$userId" with id of the user
   * @throws AccessDeniedException
   */
  public function initUserId($userStr=false) {
    $this->userId = false;
    if ($userStr) {
      $this->userId = intval(substr($userStr,4,19));
    }
    // Get the currently logged in user
    $loggedInUser = $this->securityContext->getToken()->getUser();
    $loggedInUserId = $loggedInUser->getId();
    if (!$this->userId){
        $this->userId = $loggedInUserId;
    }
    if ($this->userId == $loggedInUserId) {
      $this->user = $loggedInUser;
    } else {
      // Different user than the logged in user
      if (!$this->securityContext->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
        throw new AccessDeniedException();
      }
    }
  }

  /**
   * This will get the id of the current user
   * @return int user id
   */
  public function getUserId() {
    if (!$this->userId) {
      $this->initUserId();
    }
    return $this->userId;
  }

  /**
   * This will get the object of the current user
   * @return object user
   */
  public function getUser() {
    if (!$this->user) {
      if (!$this->userId) {
        $this->initUserId();
      }
      if (!$this->user) {
        $this->user = $this->em->getRepository('CKBundle:User')
          ->find($this->userId);
      }
    }
    return $this->user;
  }

  /**
   * Get data for use in the sidebar and the header of a main page
   *
   * @param string $table What kind of table to link to
   * @param bool|string $reqSelect Make the selected button look pressed
   * @return array template data
   */
  public function getMainPageData($table='table', $reqSelect=false)
  {
    $data = array();

    // Get the username to show in the header
    $user = $this->getUser();
    $data['UserName'] = $user->getName();
    $data['UserEmail'] = $user->getEmail();

    // Set URL to table with sorting to use in links
    $tableSortUrl = $this->router->generate('CK_default');
    $data['TableSortUrl'] = str_replace('table', $table, $tableSortUrl);

    // The mainpage tabs
    $tabs = array();
    // Get a count of the requests in different categories
    $count = $this->em->getRepository('CKBundle:Request')
      ->getCountData($this->getUserId());
    foreach (RS::getAll() as $sel) {
      $tab = array('id'=>$sel,
                   'count'=>$count[$sel]);
      // Set the right css class on the tab
      if ($reqSelect==$sel) {
        $tab['cssClass'] = 'active';
      } else {
        $tab['cssClass'] = 'normal';
      }

      $tabs[] = $tab;
    }
    $data['tabs'] = $tabs;

    // Set version info to be displayed
    $data['version'] = $this->version;

    return $data;
  }
}
