<?php
namespace FSpires\CommitKeeperBundle\Controller\Commitment;

use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Form\FormError;
use FSpires\CommitKeeperBundle\Model\TrafficLightFactoryInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Model\UserFactory as UserFactoryInterface;
use FSpires\CommitKeeperBundle\Model\StateMachineInterface;
use FSpires\CommitKeeperBundle\Model\EmailNotifierInterface;
use FSpires\CommitKeeperBundle\Model\Enum\Action;
use FSpires\CommitKeeperBundle\Entity\UserBase;

class NewPosted extends NewCommitment {
  private $userFactory;
  private $stateMachine;
  private $emailNotifier;

  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              TrafficLightFactoryInterface $trafficLightFactory,
                              DateFormatInterface $dateFormat,
                              TranslatorInterface $translator,
                              UserFactoryInterface $userFactory,
                              StateMachineInterface $stateMachine,
                              EmailNotifierInterface $emailNotifier)
  {
    parent::__construct($formFactory, $trafficLightFactory,
                        $dateFormat, $translator);
    $this->userFactory = $userFactory;
    $this->stateMachine = $stateMachine;
    $this->emailNotifier = $emailNotifier;
  }


  /**
   * Process http POST data from the form
   */
  protected function processPost(HttpRequest $httpRequest, $form, $req, $user)
  {
    $form->bind($httpRequest); // This will update the $req object
    if (!$form->isValid()) {
      return false;
    }

    $user->setCCme($req->getCCme());

    //Check if new users has been entered

    // Check requestor
    $requestor_id = $req->getRequestor()->getId();
    if (!$requestor_id) {
      $req->setRequestor(
           $this->userFactory->newContact($req->getRequestor(), $user));
    }

    // Check performer(s)
    $performers = $req->getPerformer();
    if ($performers instanceof UserBase) {
        $performers = array($performers);
    }
    if (count($performers) < 1) {
        $form->get('performer')->addError(
          new FormError('You must select at least one performer', 'newCommitment.atLeastOne')
        );
        return false;
    }

    $origReq = $req;
    $requests = array();
    foreach ($performers as $performer) {
        $req = clone $origReq;

        $performer_id = $performer->getId();
        if (!$performer_id) {
            $performer = $this->userFactory->newContact($performer, $user);
        }
        $req->setPerformer($performer);

        // Get list of allowed actions
        $actions = $this->stateMachine->getActionsForReq($req, $user);

        // Do the make action and the new request is saved to the database
        $this->stateMachine->doAction($req, $user, $actions[Action::Make]);

        // Then save the tags
        $req->saveTags();

        $requests[] = $req;
    }

    // And update the contacts
    $this->userFactory->updateContacts($user, $requests);

    return true;
  }
}
