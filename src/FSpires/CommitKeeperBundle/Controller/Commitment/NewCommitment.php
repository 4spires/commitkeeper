<?php
namespace FSpires\CommitKeeperBundle\Controller\Commitment;

use FSpires\CommitKeeperBundle\Controller\MainpageController;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use FSpires\CommitKeeperBundle\Model\TrafficLightFactoryInterface;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType as RT;
use FSpires\CommitKeeperBundle\Model\Enum\Selection as RS;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Form\Util\FormViewUtil;

class NewCommitment extends MainpageController {
  private $formFactory;
  private $trafficLightFactory;
  private $dateFormat;
  private $translator;
  /**
   * Constructor
   */
  public function __construct(FormFactoryInterface $formFactory,
                              TrafficLightFactoryInterface $trafficLightFactory,
                              DateFormatInterface $dateFormat,
                              TranslatorInterface $translator)
  {
    $this->formFactory = $formFactory;
    $this->trafficLightFactory = $trafficLightFactory;
    $this->dateFormat = $dateFormat;
    $this->translator = $translator;
  }

  /**
   * The user wants to create a new commitment
   *
   * @param HttpRequest $httpRequest
   * @param string $type One of 'request','offer' or 'toDo'
   * @param bool|int $superReqId The id of the parent request. If set, this is a supporting request
   * @return RedirectResponse|Response
   */
  public function newAction(HttpRequest $httpRequest, $type=RT::Request, $superReqId=false) {
    /**
     * Only allow paying users to make new requests.
     * This restriction is not to be here in the first version,
     * So this snippet of code is commented out.
    if (RT::Request==$type &&
        !$this->securityContext->isGranted('ROLE_PAYING_USER')) {
      $args = array('action'=>'actionNewRequest');
      $redirect = $this->router->generate('CK_upgrade_action',$args);
      return $this->redirect($redirect);
    }
    */

    $user = $this->securityContext->getToken()->getUser();
    $req = new Request();

    //Set a trafficLight factory for the new request.
    $req->setTrafficLightFactory($this->trafficLightFactory);

    // Initialize the new request
    $req->initNewReqest($type, $user, $this->em);

    $canEditCategoryValues = true;

    if ($superReqId) {
      // This is a supporting request
      $superReq = $this->em->getRepository('CKBundle:Request')
                           ->find($superReqId);
      if (!$superReq) {
        return new Response($this->translator->trans('parent.supporting.notFound'), 404);
      }

      $req->setSuperRequestId($superReqId);

      $superUsers = array();
      $superUsers[$superReq->getPerformer()->getId()]
                 = $superReq->getPerformer();
      $superUsers[$superReq->getRequestor()->getId()]
                 = $superReq->getRequestor();
      /*
      if (!array_key_exists($user->getId(),  $superUsers)) {
        return new Response(
          'User is not a participiant in the parent commitment', 401);
      }
      */
      if ($superReq->getPerformer()->getId() != $user->getId()) {
        return new Response($this->translator->trans('parent.supporting.notPerformer'), 401);
      }

      foreach ($superReq->getParentObservers() as $observer) {
        $superUsers[$observer->getId()] = $observer;
      }

      unset($superUsers[$user->getId()]);

      foreach ($superUsers as $sUser) {
        $req->addParentObserver($sUser);
      }

      $superReq->setEntityManager($this->em);
      $req->setCategories($superReq->getCategories(), true);
      $canEditCategoryValues = false;
    }

    $observersFixed = $req->getParentObservers();
    $observerChoiceList = $user->getObserverChoiceList($req, $this->em);
    $canEditObservers = (count($observerChoiceList->getChoices()) > 0);

    $contacts = $user->getContacts();

    // Get catagories from the user
    $userCategories = null;
    if ($canEditCategoryValues) {
      $userCategories = $user->getCategories($this->em);
      $emptyCategories = new ArrayCollection();
      foreach ($userCategories as $cat) {
        $ecat = clone $cat;
        $ecat->clearTags();
        $emptyCategories->set($cat->getId(), $ecat);
      }
      $req->setCategories($emptyCategories, true);
    }

    $group = $user->getGroup();
    $hasBudgetField = $group->hasBudgetField();
    $req->setHasBudgetField($hasBudgetField);
    $req->setBudgetUnit($group->getBudgetUnit());
    $req->setCCme($user->getCCme());

    // Make the form
    $form = $this->formFactory->create('commitmentNew', $req,
              array('reqType'=>$type,
                    'hasBudgetField' => $hasBudgetField,
                    'contacts'       => $contacts,
                    'observersFixed' => $observersFixed,
                    'observerChoiceList' => $observerChoiceList,
                    'canEditObservers' => $canEditObservers,
                    'canEditCategoryValues' => $canEditCategoryValues,
                    'userCategories'=>$userCategories,
                    'dateFormat'=>$this->dateFormat));

    if ($superReqId) {
      $superReqUrl = $this->router->generate(
                   'CK_commitment_id',
                   array('commitmentIdStr'=>$superReq->getId()));
    }

    // Do the processPost method to handle any posted data
    if ($this->processPost($httpRequest, $form, $req, $user)) {
      if ($superReqId) {
        $returnUrl = $superReqUrl;
      } else {
        // Go to the tab "Due From Others" after
        // sucessfully having created a new commitment,
        // But go to "Due From Me" when making a toDo
        $reqSelect = RS::PendingByOthers;
        if (RT::ToDo == $type) {
          $reqSelect = RS::PendingByMe;
        }
        $returnUrl = $this->makeTableUrl('table', $reqSelect);
      }
      return $this->redirect($returnUrl);
    }

    // Template data
    $templData = $this->getMainPageData();
    $templData['form'] = $form->createView();
    $templData['form_action'] = $httpRequest->getRequestUri();
    $templData['returnUrl'] = $this->getReturnUrl($user);

    $needNewContactUrl = false;
    if (RT::Request == $type) {
      $templData['formNewContactP'] = $this->makeNewContactFormView();
      $needNewContactUrl = true;
    }
    $templData['canEditCategoryValues'] = $canEditCategoryValues;
    $templData['canEditObservers'] = $canEditObservers;
    if ($canEditObservers) {
      $templData['formNewContactO'] = $this->makeNewContactFormView();
      $needNewContactUrl = true;
    } else {
      $templData['ObserverList'] =  $req->getObserverList($this->translator);
    }

    if ($needNewContactUrl) {
      $templData['newContactUrl'] = $this->router->generate('CK_newContact');
    }

    if ($superReqId) {
      $templData['superReq'] = $superReq;
      $templData['superReqUrl'] = $superReqUrl;
      $templData['returnUrl'] = $superReqUrl;
    } else {
      $templData['superReq'] = false;
    }
    $templData['req'] = $req;
    $templData['trafficLight'] = $req->getTrafficLight();
    $templData['type'] = $type;
    $templData['hasBudgetField'] = $hasBudgetField;
    if ($hasBudgetField) {
      $templData['budgetUnit'] = $group->getBudgetUnit()->getUnit();
    }

    // jsDatePick data
    $templData += $this->dateFormat
      ->getDatePickerVars($req->getNewDueDate(), 'commitmentNew_newDueDate');

    // Attachments and Links
    $prototype = $templData['form']['newAttachments']->vars['prototype'];
    $templData['newAttachmentPrototype'] = FormViewUtil::copy($prototype);

    // Send the data to a twig template for viewing
    $templData['HasOnload'] = true;
    $templData['pageClass'] = 'new-commitment';
    return $this->render('CKBundle:Commitment:new.html.twig', $templData);
  }

  // New contact ajax form
  private function makeNewContactFormView()
  {
      $formNewContact = $this->formFactory->create('user_name_input', null,
          array('virtual'  => false, 'required'=>false));
      return $formNewContact->createView();
  }

  /**
   * Process http POST data from the form
   */
  protected function processPost(HttpRequest $httpRequest, $form, $req, $user)
  {
    // This class handles only the GET method
    return false;
  }
}
