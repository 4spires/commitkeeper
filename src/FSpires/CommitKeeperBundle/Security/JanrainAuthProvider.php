<?php
namespace FSpires\CommitKeeperBundle\Security;

use FSpires\CommitKeeperBundle\Security\OpenIdUserProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;

/**
 * JanrainAuthProvider uses an OpenIdUserProviderInterface
 * to retrieve the user for a JanrainUserToken.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Terje Bråten <terje@braten.be>
 */
class JanrainAuthProvider implements AuthenticationProviderInterface
{
  private $hideUserNotFoundExceptions;
  private $userChecker;
  private $providerKey;

  private $userProvider;
  private $userOpenProvider;

  private $auth_info_url;
  private $janrainkey;


  /**
   * Constructor.
   * @param OpenIdUserProviderInterface $userOpenProvider       An OpenIdUserProviderInterface instance
   * @param UserProviderInterface   $userProvider               An UserProviderInterface instance
   * @param UserCheckerInterface    $userChecker                An UserCheckerInterface instance
   * @param string                  $providerKey                The provider key
   * @param string                  $janrainkey                 The Janrain api key
   * @param Boolean                 $hideUserNotFoundExceptions Whether to hide user not found exception or not
   */
  public function __construct(
             OpenIdUserProviderInterface $userOpenProvider,
             UserProviderInterface $userProvider,
             UserCheckerInterface $userChecker,
             $providerKey,
             $janrainkey,
             $hideUserNotFoundExceptions = true)
  {
    if (empty($providerKey)) {
      throw new \InvalidArgumentException('$providerKey must not be empty.');
    }

    $this->userChecker = $userChecker;
    $this->providerKey = $providerKey;
    $this->hideUserNotFoundExceptions = $hideUserNotFoundExceptions;

    $this->userProvider = $userProvider;
    $this->userOpenProvider = $userOpenProvider;

    $this->janrainkey = $janrainkey;
    $this->auth_info_url = 'https://rpxnow.com/api/v2/auth_info';
  }


  /**
   * {@inheritdoc}
   */
  public function authenticate(TokenInterface $token)
  {
    if (!$this->supports($token)) {
      return null;
    }

    $auth_info = $this->getJanrainAuthInfo($token->getJToken());

    // Check status on $auth_info
    if ('ok' != $auth_info['stat']) {
      $msg = 'Authentication error';
      try {
        $msg = $auth_info['err']['msg'];
      } catch (\Exception $ex) {}
      throw new BadCredentialsException($msg);
    }

    $openIdData  = $auth_info['profile'];
    $tmpToken = new JanrainUserToken('', 'no provider key', $openIdData);

    try {
      // Try to attach the temporary token to an user
      $user = $this->retrieveUser($tmpToken);

      $this->userChecker->checkPreAuth($user);
      $this->userChecker->checkPostAuth($user);

      // Return an authenticated token
      return new JanrainUserToken($user, $this->providerKey,
                                    $openIdData, $user->getRoles());
    } catch (UsernameNotFoundException $notFound) {
      if ($this->hideUserNotFoundExceptions) {
        throw new BadCredentialsException('Bad credentials', 0, $notFound);
      }
      throw $notFound;
    }
  }

  /**
   * Retrieves the user from an implementation-specific location.
   *
   * @param JanrainUserToken $token The token
   *
   * @return UserInterface object representing the user
   *
   * @throws AuthenticationException if the credentials could not be validated
   */
  protected function retrieveUser(JanrainUserToken $token)
  {
    try {
      // First we see if the OpenId user provider has a user for us
      $user = $this->userOpenProvider->loadUserByToken($token);
      // It may return just an user name as a string

      // If it is a string check if the ordinary user provider can give a user
      if (is_string($user)) {
        $user = $this->userProvider->loadUserByUsername($user);
      }

      if (!$user instanceof UserInterface) {
        throw new AuthenticationServiceException('The user provider must return a UserInterface object.', $token);
      }

      return $user;
    } catch (UsernameNotFoundException $notFound) {
      throw $notFound;
    } catch (\Exception $repositoryProblem) {
      throw new AuthenticationServiceException($repositoryProblem->getMessage(), $token, 0, $repositoryProblem);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function supports(TokenInterface $token)
  {
    return $token instanceof JanrainUserToken && $this->providerKey === $token->getProviderKey();
  }


  /**
   * Pass the janrain token to the authentication service
   * @return array An array with data from
   *               the janrain authentication service
   */
  protected function getJanrainAuthInfo($JToken)
  {
    $post_data = array('token'  => $JToken,
                       'apiKey' => $this->janrainkey,
                       'format' => 'json');

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, $this->auth_info_url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FAILONERROR, true);

    $raw_json = curl_exec($curl);
    if ($raw_json == false){
      $err_code = curl_errno($curl);
      $err_msg = 'Curl error: ' . curl_error($curl)
        . "\n- Error code: " . $err_code;
      $extra_info = "\n- URL: " . $this->auth_info_url
        . "\n- token: " . $JToken;
      curl_close($curl);
      throw new CurlException($err_msg . $extra_info, $err_code);
    }
    curl_close($curl);

    return json_decode($raw_json, true);
  }
}
