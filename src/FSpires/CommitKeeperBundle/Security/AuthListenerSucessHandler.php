<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use FSpires\CommitKeeperBundle\Entity\AnonymousUser;
use Symfony\Component\Security\Core\Role\Role;

/**
 * AuthListenerSucessHandler, a custom authentication success handler
 *
 * @author Terje Bråten <terje@braten.be>
 */
class AuthListenerSucessHandler implements AuthenticationSuccessHandlerInterface
{
   private $httpUtils;
   private $em;
   private $options;

    /**
     * Constructor
     *
     * @param HttpUtils $httpUtils
     * @param EntityManager $em
     * @param array $options
     */
   public function __construct(HttpUtils $httpUtils, 
                               EntityManager $em,
                               array $options = array())
   {
     $this->httpUtils = $httpUtils;
     $this->em = $em;
     $default_sucess_opts = array(
            'always_use_default_target_path' => false,
            'default_target_path'            => '/',
            'target_path_parameter'          => '_target_path',
            'use_referer'                    => false,
            'anonymous_path'                 => false
                                  );
     $this->options = array_merge($default_sucess_opts, $options);
   }

   /**
    * Check if a role is in the array of roles
    * @param $roleToCheck string Role to check for
    * @param array of valid roles
    *
    * @return boolean True if the role is found
    */
   private function hasRole($roleToCheck, $roles)
   {
     foreach ($roles as $role) {
       if ($role instanceof Role) {
         $role = $role->getRole();
       }
       if ($role==$roleToCheck) {
         return true;
       }
     }

     return false;
   }
 
   /**
    * This is called when an interactive authentication attempt succeeds. This
    * is called by authentication listeners inheriting from AbstractAuthenticationListener.
    * @param Request        $request
    * @param TokenInterface $token
    * @return Response The response to return
    */
   function onAuthenticationSuccess(Request $request, TokenInterface $token): ?Response
   {
     $targetUrl = false;

     if ($this->options['always_use_default_target_path']) {
       $targetUrl = $this->options['default_target_path'];
     }

     $session = $request->getSession();
     $roles = $token->getRoles();
     if (count($roles)>0) {
       if ($this->hasRole('ROLE_TYPE1', $roles)) {
         $commitmentId = intval($request->query->get('c'));
         if ($commitmentId>0) {
           $request->getSession()->set('loginCommitmentId', $commitmentId);
         } else {
           $targetUrl = 'CK_confirm';
         }
       } else if ($this->hasRole('ROLE_TYPE2', $roles)) {
         $targetUrl = 'CK_confirm';
       } else if ($this->hasRole('ROLE_ANONYMOUS', $roles)) {
         if (!$targetUrl && $this->options['anonymous_path']) {
           $targetUrl = $this->options['anonymous_path'];
         }
       } else {
         // We have a valid normal login
         // Add any open Id provediers from the anonymous user
         // to this logged in user
         $anon_user = AnonymousUser::getFromSession($session);
         $openIds = $anon_user->getOpenIdentifier();
         if (count($openIds)>0) {
           $user = $token->getUser();
           foreach ($openIds as $oid) {
             $user->addOpenIdentifier($oid);
           }
           $user->setUpdatedDate();
           $this->em->flush();
           $anon_user->removeFromSession();
         }
       }
     }

     if (!$targetUrl) {
       $targetUrl = $request->get($this->options['target_path_parameter'],
                                  null, true);
     }
     if (!$targetUrl) {
       if ($targetUrl = $session->get('_security.secured_area.target_path')) {
           $session->remove('_security.secured_area.target_path');
       }
     } else {
       if ($session->get('_security.secured_area.target_path')) {
         $session->remove('_security.secured_area.target_path');
       }
     }
     if (!$targetUrl) {
       if ($this->options['use_referer']) {
         $targetUrl = $request->headers->get('Referer');
       }
     }
     if (!$targetUrl || false!==strpos($targetUrl,'login')) {
       $targetUrl = $this->options['default_target_path'];
     }

     return $this->httpUtils->createRedirectResponse($request, $targetUrl);
   }
}
