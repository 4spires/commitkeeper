<?php
namespace FSpires\CommitKeeperBundle\Security;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use FSpires\CommitKeeperBundle\Security\OpenIdUserProviderInterface;

/**
 * Provides users from an Open Id identifier.
 *
 * If a property is provided in the constuctor it will use
 * the doctrine repository of the given entity class
 * to find an entity of that class that has the given property matching
 * the Open Id identifier from the JanrainUserToken.
 *
 * If no property is given, the doctrine repository of the entity class
 * must implement the OpenIdUserProviderInterface.
 */
class JanrainUserProvider implements OpenIdUserProviderInterface
{
  private $entityClass;
  private $repository;
  private $property;

  /**
   * Constructor.
   * @param Doctrine\ORM\EntityManager  $em           A Doctrine EntityManager instance
   * @param string                      $entityClass  The class of the entity
   * @param string                      $property     A property to search
   */
  public function __construct(EntityManager $em, $entityClass, $property=null)
  {
    if (false !== strpos($entityClass, ':')) {
      // Go to the metadata to find the class name
      $metadata = $em->getClassMetadata($entityClass);
      $entityClass = $metadata->name;
    }
    $this->repository = $em->getRepository($entityClass);
    $this->property = $property;
    $this->entityClass = $entityClass;
  }

  /**
   * {@inheritdoc}
   */
  public function loadUserByToken(JanrainUserToken $token)
  {
    if (null !== $this->property) {
      $openIdentifier =  $token->getCredentials();
      $user = $this->repository->findOneBy(
                        array($this->property => $openIdentifier));
    } else {
      if (!$this->repository instanceof OpenIdUserProviderInterface) {
        throw new \InvalidArgumentException(
 sprintf('The "%s"\'s repository class "%s" must implement OpenIdUserProviderInterface.', $this->entityClass, get_class($this->repository)
)
                                            );
      }
      $user = $this->repository->loadUserByToken($token);
    }

    if (null === $user) {
      throw new UsernameNotFoundException(
                    sprintf('User with openId "%s" not found.',
                            $token->getCredentials()
                            )
                                          );
    }
    return $user;
  }
}
