<?php

namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class CustomBadCredentialsException extends BadCredentialsException
{
  private $messageKey;
  private $messageData;

  public function __construct($messageKey, array $messageData = array())
  {
    $this->messageKey = $messageKey;
    $this->messageData = $messageData;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageKey(): string
  {
    return $this->messageKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageData(): array
  {
    return $this->messageData;
  }

  public function serialize()
  {
    return serialize(array(
      $this->messageKey,
      $this->messageData,
      parent::serialize()
    ));
  }

  public function unserialize($str)
  {
    list(
      $this->messageKey,
      $this->messageData,
      $parentData
      ) = unserialize($str);
    parent::unserialize($parentData);
  }
}
