<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\SessionUnavailableException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Role\Role;
use FSpires\CommitKeeperBundle\Security\CustomBadCredentialsException;
use FSpires\CommitKeeperBundle\Model\UserFactory AS UserFactoryInterface;

/**
 * AuthenticationListener is an implementation of
 * an authentication via a form composed of a username and a password.
 * Extended to also include janrain authentication
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Terje Bråten <terje@braten.be>
 */
class AuthenticationListener extends CkAbstractAuthenticationListener
{
  private $userFactory;
  private $csrfProvider;
  private $janrainAuthProvider;
  private $securityContext;

  /**
   * {@inheritdoc}
   */
  public function __construct(
                SecurityContextInterface $securityContext,
                AuthenticationManagerInterface $authenticationManager,
                SessionAuthenticationStrategyInterface $sessionStrategy,
                HttpUtils $httpUtils,
                $providerKey,
                AuthenticationSuccessHandlerInterface $successHandler = null,
                AuthenticationFailureHandlerInterface $failureHandler = null,
                array $options = array(),
                LoggerInterface $logger = null,
                EventDispatcherInterface $dispatcher = null,
                UserFactoryInterface $userFactory = null,
                AuthenticationManagerInterface $janrainManager=null,
                CsrfProviderInterface $csrfProvider = null
                                )
  {
    parent::__construct(
          $securityContext,
          $authenticationManager,
          $sessionStrategy,
          $httpUtils,
          $providerKey,
          $successHandler,
          $failureHandler,
          array_merge(array(
            'username_parameter' => '_username',
            'password_parameter' => '_password',
            'cnonce_parameter'   => '_cnonce',
            'csrf_parameter'     => '_csrf_token',
            'anonymous_path'     => '',
            'intention'          => 'authenticate',
            'post_only'          => true,
                            ), $options),
          $logger,
          $dispatcher
                            );

    $this->userFactory = $userFactory;
    $this->csrfProvider = $csrfProvider;
    $this->janrainAuthProvider = $janrainManager;
    $this->securityContext = $securityContext;
  }

  /**
   * Check if a role is in the array of roles
   * @param $roleToCheck string Role to check for
   * @param array of valid roles
   *
   * @return boolean True if the role is found
   */
  private function hasRole($roleToCheck, $roles)
  {
    foreach ($roles as $role) {
      if ($role instanceof Role) {
        $role = $role->getRole();
      }
      if ($role==$roleToCheck) {
        return true;
      }
    }

    return false;
  }

  /**
   * {@inheritdoc}
   */
  protected function attemptAuthentication(Request $request)
  {
    $fromEmail = false;
    $forgotPW = false;
    $resetPW = false;
    if ($newUser = $request->query->get('u')) {
      if (!(strpos($newUser,':') > 1)) {
        throw new BadCredentialsException('Invalid user string');
      }

      if ($request->query->get('r')) {
        $resetPW = true;
      } else {
        $fromEmail = true;
      }

      list($username, $passwd) = explode(':', $newUser, 2);
      $username = base64_decode($username);

      $commitmentId = intval($request->query->get('c'));
      $targetUrl = false;
      if ($commitmentId>0) {
        $targetUrl = '/' . $request->getLocale() . '/commitment/' . $commitmentId;
      }

      $token = $this->securityContext->getToken();
      if ($token && $token->getUsername()==$username &&
          $this->hasRole('ROLE_TYPE3', $token->getRoles())) {
        if (!$targetUrl) {
          $targetUrl = 'CK_default';
        }
        return $this->httpUtils->createRedirectResponse($request, $targetUrl);
      }

      if ($targetUrl) {
        $request->getSession()->set('_security.secured_area.target_path', $targetUrl);
      }
    } else {
      if (!$request->hasPreviousSession()) {
        throw new SessionUnavailableException('Your session has timed out, or you have disabled cookies.');
      }

      if ($this->options['post_only'] && 'post' !== strtolower($request->getMethod())) {
        if (null !== $this->logger) {
          $this->logger->debug(sprintf('Authentication method not supported: %s.', $request->getMethod()));
        }

        return null;
      }

      if (null !== $this->janrainAuthProvider) {
        // Check for janrain token
        $jtoken = $request->request->get('token');
        if (strlen($jtoken) == 40) {
          $janrainToken = new JanrainUserToken($jtoken, $this->providerKey);
          return $this->janrainAuthProvider->authenticate($janrainToken);
        }
      }

      if (null !== $this->csrfProvider) {
        $csrfToken = $request->request->get($this->options['csrf_parameter'], null, true);
        if (false === $this->csrfProvider->isCsrfTokenValid($this->options['intention'], $csrfToken)) {
          throw new InvalidCsrfTokenException('login.invalidCSRFtoken');
        }
      }

      $username = trim($request->request->get($this->options['username_parameter'], null, true));
      if (!$username) {
        throw new BadCredentialsException('login.noEmail');
      }
      if ($request->request->get('forgotPW', false)) {
        $this->userFactory->passwordReset($username);
        $forgotPW = true;
      } else {
        $passwd = $request->request->get($this->options['password_parameter'], null, true);
      }
    }

    $request->getSession()->set(SecurityContextInterface::LAST_USERNAME, $username);
    if ($forgotPW) {
      throw new CustomBadCredentialsException('login.sentReminder%email%', array('%email%' => $username));
    }

    $token = new UsernamePasswordToken($username, $passwd, $this->providerKey);
    if ($fromEmail) {
      $token->setAttribute('FromEmail', true);
    }
    if ($resetPW) {
      $token->setAttribute('Reset', true);
    }
    return $this->authenticationManager->authenticate($token);
  }
}
