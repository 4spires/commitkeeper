<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;

class CurlException extends AuthenticationServiceException {}
