<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;


/**
 * FormLoginFactory creates services for form login authentication.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Johannes M. Schmitt <schmittjoh@gmail.com>
 * @author Terje Bråten <terje@braten.be>
 */
class FormLoginFactory extends AbstractFactory
{
    public function getPriority(): int
    {
        return 0;
    }

    public function createAuthenticator(ContainerBuilder $container, string $firewallName, array $config, string $userProviderId): string|array
    {
        // TODO: Implement createAuthenticator() method.
    }

    public function __construct()
    {
        $this->addOption('username_parameter', '_username');
        $this->addOption('password_parameter', '_password');
        $this->addOption('csrf_parameter', '_csrf_token');
        $this->addOption('anonymous_path', '');
        $this->addOption('intention', 'authenticate');
        $this->addOption('post_only', true);
    }

    public function getPosition()
    {
        return 'form';
    }

    public function getKey(): string
    {
        return 'form-login-ck';
    }

    public function addConfiguration(NodeDefinition $node)
    {
      parent::addConfiguration($node);

      $node
        ->children()
          ->scalarNode('csrf_provider')->cannotBeEmpty()->end()
          ->scalarNode('janrain_api_key')->isRequired()->cannotBeEmpty()->end()
          ->arrayNode('openid_user')->isRequired()->children()
            ->scalarNode('provider')->defaultValue('janrain.user.provider')
               ->cannotBeEmpty()->end()
            ->scalarNode('class')->isRequired()->cannotBeEmpty()->end()
            ->scalarNode('property')->defaultNull()->end()
          ->end()
        ->end()
        ;
    }

    protected function getListenerId()
    {
        return 'ck.security.authentication.listener';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
      $providerDef = 'ck.security.authentication.provider';
      $providerId = $providerDef .'.'. $id;

      $container
        ->setDefinition($providerId, new DefinitionDecorator($providerDef))
        ->replaceArgument(0, new Reference($userProviderId))
        ->replaceArgument(2, $id)
        ;

      return $providerId;
    }


    protected function createJanrainProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
      $providerDef = 'janrain.authentication.provider';
      $providerId = $providerDef .'.'. $id;

      $confOpenUser = $config['openid_user'];
      $openUserProviderId = $confOpenUser['provider'];

      $container
        ->setDefinition($providerId, new DefinitionDecorator($providerDef))
        ->replaceArgument(0, new Reference($openUserProviderId))
        ->replaceArgument(1, new Reference($userProviderId))
        ->replaceArgument(3, $id)
        ;

      $container->setParameter('janrain.api.key', $config['janrain_api_key']);
      $container->setParameter('janrain.user.class', $confOpenUser['class']);
      $container->setParameter('janrain.user.property', $confOpenUser['property']);

      return $providerId;
    }

    protected function createListener($container, $id, $config, $userProviderId)
    {
      $listenerId = parent::createListener($container, $id, $config, $userProviderId);
      $defineListener = $container->getDefinition($listenerId);

      // custom success handler
      if (isset($config['success_handler'])) {
        $sucess_opts = array(
            'always_use_default_target_path' => false,
            'default_target_path'            => '/',
            'target_path_parameter'          => '_target_path',
            'use_referer'                    => false,
            'anonymous_path'                 => false
                             );
        $sucess_opts = array_intersect_key($config, $sucess_opts);
        $sucess_handler_id = $config['success_handler'] . '.' . $id;
        $container
          ->setDefinition($sucess_handler_id,
                         new DefinitionDecorator($config['success_handler']))
          ->addArgument($sucess_opts);
        $defineListener->replaceArgument(5, new Reference($sucess_handler_id));
      }

      // Add a janrain provider
      $jprovider = $this->createJanrainProvider($container, $id, $config, $userProviderId);

      $defineListener->addArgument(new Reference($jprovider));

      if (isset($config['csrf_provider'])) {
        $defineListener->addArgument(new Reference($config['csrf_provider']));
      }

      return $listenerId;
    }

    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
        $entryPointId = 'security.authentication.form_entry_point.'.$id;
        $container
            ->setDefinition($entryPointId, new DefinitionDecorator('security.authentication.form_entry_point'))
            ->addArgument(new Reference('security.http_utils'))
            ->addArgument($config['login_path'])
            ->addArgument($config['use_forward'])
        ;

        return $entryPointId;
    }
}
