<?php
namespace FSpires\CommitKeeperBundle\Security;

use Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use FSpires\CommitKeeperBundle\Entity\UserRepository;

/**
 * Authentication provider specific to CommitKeeper
 */
class AuthenticationProvider extends DaoAuthenticationProvider
{
  private $encoderFactory;
  private $em;

  /**
   * Constructor.
   *
   * @param UserProviderInterface $userProvider An UserProviderInterface instance
   * @param UserCheckerInterface $userChecker An UserCheckerInterface instance
   * @param string $providerKey The provider key
   * @param EncoderFactoryInterface $encoderFactory An EncoderFactoryInterface instance
   * @param Boolean $hideUserNotFoundExceptions Whether to hide user not found exception or not
   * @param \Doctrine\Common\Persistence\ObjectManager $em
   */
  public function __construct(UserProviderInterface $userProvider, UserCheckerInterface $userChecker, $providerKey, EncoderFactoryInterface $encoderFactory, $hideUserNotFoundExceptions = true, EntityManagerInterface $em = null)
  {
    parent::__construct($userProvider, $userChecker, $providerKey, $encoderFactory, $hideUserNotFoundExceptions);
    $this->encoderFactory = $encoderFactory;
    $this->em = $em;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAuthentication(UserInterface $user, UsernamePasswordToken $token)
  {
    if ($token->hasAttribute('FromEmail') && 1 != $user->getUserType()) {
      throw new BadCredentialsException('WrongEmailUser');
    }

    $postedPwd = $token->getCredentials();
    if ('' == $postedPwd) {
      throw new BadCredentialsException('login.password.empty');
    }

    $pwInvalidMsg = 'login.password.invalid';
    if (1 == $user->getUserType()) {
      if ($user->getPassword() != $postedPwd) {
        throw new BadCredentialsException($pwInvalidMsg);
      }
      return;
    }

    $PWencoder = $this->encoderFactory->getEncoder($user);
    if ($PWencoder->isPasswordValid($user->getPassword(), $postedPwd, null)) {
      return;
    }

    /** @var UserRepository $urepo */
    $urepo = $this->em->getRepository('CKBundle:User');
    $tmpPasswords = $urepo->findTemporaryPasswords($user->getId());
    foreach ($tmpPasswords as $id => $tmpPasswd) {
      if ($PWencoder->isPasswordValid($tmpPasswd, $postedPwd, null)) {
        $user->setUserType(2);
        $user->setPassword($tmpPasswd);
        $user->setUpdatedDate();
        $this->em->flush();
        $urepo->usedTemporaryPassword($id, $user->getUpdatedDate());
        return;
      }
    }

    throw new BadCredentialsException($pwInvalidMsg);
  }
}
