<?php

namespace FSpires\CommitKeeperBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use FSpires\CommitKeeperBundle\Entity\UserRepository;
use FSpires\CommitKeeperBundle\Entity\User;
use FSpires\CommitKeeperBundle\Entity\RequestRepository;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Entity\History;
use FSpires\CommitKeeperBundle\Model\TrafficLightFactoryInterface;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight;

class CheckDueDate extends Command
{
  const SYSTEM_USER_ID = 1;

  /**
   * @var EntityManagerInterface
   */
  private $em;

  /**
   * @var TrafficLightFactoryInterface
   */
  private $trafficLightFactory;

  /**
   * @var string CommitKeeper revision for the History items
   */
  private $revision;

  /**
   * @var OutputInterface
   */
  private $output;

  /**
   * Constructor
   *
   * @param EntityManagerInterface $em
   * @param TrafficLightFactoryInterface $trafficLightFactory
   * @param array $version
   */
  public function __construct(
      EntityManagerInterface $em,
      TrafficLightFactoryInterface $trafficLightFactory,
      array $version
  ) {
    $this->em = $em;
    $this->trafficLightFactory = $trafficLightFactory;
    $this->revision = $version['revno'];

    parent::__construct();
  }

  protected function configure()
  {
    date_default_timezone_set('UTC');
    $this
      ->setName('ck:check-due-date')
      ->setDescription('Check the due Date on commitments and update the trafficlight if they are past')
      ->addOption(
        'dry-run',
        'd',
        InputOption::VALUE_NONE,
        'Do not update any trafficlights or change the database, just show what would have been done.'
      )
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    $this->output = $output;
    $dryRun = $input->getOption('dry-run');
    /** @var User $systemUser */
    $systemUser = $this->getSystemUser();
    if (!$systemUser) {
      return;
    }

    /** @var RequestRepository $requestRepository */
    $requestRepository = $this->em->getRepository('CKBundle:Request');

    $commitments = $requestRepository->findPastDueDate();
    if (!$commitments) {
      $this->log('No commitments found that was past their due date');
      return;
    }

    foreach ($commitments as $commitment) {
      /** @var Request $commitment */

      $this->log(sprintf('Updating commitment id=%d title="%s" dueDate=%s, status="%s"',
          $commitment->getId(),
          $commitment->getTitle(),
          $commitment->getDueDate()->format('Y-m-d'),
          $commitment->getTrafficLight()->getTitle()
      ));

      if (!$dryRun) {
        $commitment->setStatusUpdate(TrafficLight::OffTrack);
        $commitment->setTrafficLightFactory($this->trafficLightFactory);
        $commitment->updateTrafficLight();
        $commitment->setDescription($commitment->getTrafficLight()->getStatusMsg());

        $history = new History();
        $history->setRevision($this->revision);
        $history->setFromRequest($commitment);
        $history->setDueDatePrevious($commitment->getDueDate());
        if ($commitment->hasBudgetField()) {
          $history->setBudgetPrevious($commitment->getBudget());
        }
        $history->setPhaseChanged(false);
        $history->setTrafficLightChanged(false); // We do not want an extra message about the change
        $history->setActionStr('AutomaticStatusChange');
        $history->setActor($systemUser);
        $history->setRole('S');
        $this->em->persist($history);
      }
    }

    if (!$dryRun) {
      $this->em->flush();
    }
  }

  private function log($message, $level = 'info')
  {
    $this->output->writeln("<$level>$message</$level>");
  }

  /**
   * @return null|object
   */
  private function getSystemUser()
  {
    /** @var UserRepository $userRepository */
    $userRepository = $this->em->getRepository('CKBundle:User');

    $user = $userRepository->find(self::SYSTEM_USER_ID);
    if (!$user) {
      $this->log('System user not found', 'error');
    }
    return $user;
  }
}
