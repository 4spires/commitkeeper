<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;

/**
 * FSpires\CommitKeeperBundle\Entity\UserName
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class UserName extends UserBase
{
  public function __construct($id=null)
  {
    $this->id = $id;
  }

  /**
   * Filter a user object, make sure it is a UserName object
   *
   * @param  \FSpires\CommitKeeperBundle\Entity\UserBase $user
   * @param  EntityManagerInterface $em
   * @throws \InvalidArgumentException
   * @return \FSpires\CommitKeeperBundle\Entity\UserName
   */
  static public function filter(UserBase $user, EntityManagerInterface $em) {
    if (!($user instanceof UserName)) {
      if (!$em) {
        throw new \InvalidArgumentException('EntityManager must be set');
      }
      $userId = $user->getId();
      if (!$userId) {
        throw new \InvalidArgumentException('User id must be set');
      }
      $old_user = $user;
      $user = $em->getPartialReference(
         'FSpires\CommitKeeperBundle\Entity\UserName', $userId);
      $old_user->copyToUser($user);
    }
    return $user;
  }
}
