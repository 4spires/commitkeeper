<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FSpires\CommitKeeperBundle\Entity\UserBase
 * Abstract base class for the classes User and UserName
 *
 * @ORM\Table(name="user")
 * @ORM\MappedSuperclass
 */
class UserBase implements \Serializable
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  protected $id;

  /**
   * @var string $firstName
   *
   * @ORM\Column(name="first_name", type="string", length=100, nullable=false)
   * @Assert\NotBlank(message="Please enter a first name")
   */
  private $firstName;

  /**
   * @var string $lastName
   *
   * @ORM\Column(name="last_name", type="string", length=100, nullable=false)
   * @Assert\NotBlank(message="Please enter a last name")
   */
  private $lastName;

  /**
   * @var string $email
   *
   * @ORM\Column(name="email", type="string", length=200, nullable=false)
   * @Assert\NotBlank(message="Please enter an email")
   * @Assert\Email(
   *     message = "The email '{{ value }}' is not a valid email.",
   *     checkHost = true
   * )
   */
  protected $email;

  /**
   * Get userId
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set firstName
   *
   * @param string $firstName
   */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;
  }

  /**
   * Get firstName
   *
   * @return string 
   */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
   * Set lastName
   *
   * @param string $lastName
   */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;
  }

  /**
   * Get lastName
   *
   * @return string 
   */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
   * Set email
   *
   * @param string $email
   */
  public function setEmail($email)
  {
    $this->email = $email;
  }

  /**
   * Get email
   *
   * @return string 
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Get Name
   *
   * @return string 
   */
  public function getName()
  {
    $name = $this->firstName . ' ' . $this->lastName;
    return trim($name);
  }

  /**
   * Get Name and Email
   *
   * @return string 
   */
  public function getNameAndEmail()
  {
    $name = $this->getName();
    return $name . ' <' . $this->email . '>';
  }

  /**
   * Get array representing the email
   *
   * @return array
   */
  public function getEmailArray()
  {
    return array($this->email => $this->getName());
  }

  /**
   * Copy all data in this UserBase object
   * to the given User object
   */
  public function copyToUser(UserBase &$user)
  {
    $user->setFirstName($this->firstName);
    $user->setLastName($this->lastName);
    $user->setEmail($this->email);
  }

  public function serialize()
  {
    return serialize(array(
                 $this->id,
                 $this->firstName,
                 $this->lastName,
                 $this->email
                           ));
  }

  public function unserialize($str)
  {
    list(
                 $this->id,
                 $this->firstName,
                 $this->lastName,
                 $this->email
         ) = unserialize($str);
  }
}
