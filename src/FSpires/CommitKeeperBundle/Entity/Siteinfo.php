<?php

namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSpires\CommitKeeperBundle\Entity\Siteinfo
 *
 * @ORM\Table(name="siteInfo")
 * @ORM\Entity
 */
class Siteinfo
{
    /**
     * @var string $id
     *
     * @ORM\Column(name="id", type="string", length=32, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var text $value
     *
     * @ORM\Column(name="value", type="text", nullable=false)
     */
    private $value;



    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param text $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return text 
     */
    public function getValue()
    {
        return $this->value;
    }
}