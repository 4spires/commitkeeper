<?php

namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\EntityRepository;
use FSpires\CommitKeeperBundle\Security\OpenIdUserProviderInterface;
use FSpires\CommitKeeperBundle\Security\JanrainUserToken;

class OpenIdentifierRepository extends EntityRepository implements OpenIdUserProviderInterface
{
  /**
   * {@inheritdoc}
   */
  public function loadUserByToken(JanrainUserToken $token)
  {
    $openIdentifier =  $token->getCredentials();
    $identifier = $this->find($openIdentifier);
    if ($identifier) {
      return $identifier->getUser();
    }
    return 'OPENID_ANONYMOUS';
  }
}
