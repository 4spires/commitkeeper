<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FSpires\CommitKeeperBundle\Security\JanrainUserToken;

/**
 * FSpires\CommitKeeperBundle\Entity\AnonymousUser
 *
 * A temporary user not stored in the database
 * but is stored in the session
 */
class AnonymousUser extends UserBase
{
  /**
   * A copy of the symfony 2 session object
   */
  private $session;

  /**
   * A collection of OpenIdentifier objects
   * @var \Doctrine\Common\Collections\ArrayCollection $openIdentifier
   */
  private $openIdentifier;

  /**
   * An array of possible emails for this user
   * @var array $emails
   */
  private $emails;


  /**
   * Get the anonymous user from the session
   */
  public static function getFromSession($session) {
    $serStr = $session->get('anonymous_user');
    if (!empty($serStr)) {
      $AnonUser = unserialize($serStr);
      $AnonUser->session = $session;
    } else {
      $AnonUser = new AnonymousUser($session);
    }
    return $AnonUser;
  }

  /**
   * Constructor
   *
   * @param object Session object from symfony 2
   */
  public function __construct($session) {
    $this->session = $session;

    $this->openIdentifier = new ArrayCollection();
    $this->emails = array();
  }

  /**
   * Put this anonymous user into the session
   */
  public function saveInSession() {
    $this->session->set('anonymous_user', serialize($this));
  }

  /**
   * Remove the anonymous user from the session
   */
  public function removeFromSession() {
    $this->session->remove('anonymous_user');
  }

  /**
   * Copy all data in this Anonymous user object
   * to the given User object
   */
  public function copyToUser(UserBase &$user)
  {
    parent::copyToUser($user);
    $user->setOpenIdentifier($this->openIdentifier);
  }

  /**
   * Add data to the anonymous user from a JanrainUserToken
   * @param \FSpires\CommitKeeperBundle\Security\JanrainUserToken $token
   */
  public function addDataFromToken(JanrainUserToken $token)
  {
    $openIdData = $token->getOpenIdData();
    if (empty($openIdData)) {
      return;
    }
    if (!array_key_exists('identifier', $openIdData)) {
      return;
    }

    // search for duplicate identifiers
    $identifier = $openIdData['identifier'];
    foreach ($this->openIdentifier as $oid) {
      if ($oid->getIdentifier()===$identifier) {
        return;
      }
    }

    // Get the persons name from the openId data
    if (array_key_exists('name', $openIdData)) {
      $name =  $openIdData['name'];
      if (array_key_exists('givenName', $name)) {
        $this->setFirstName($name['givenName']);
      }
      if (array_key_exists('familyName', $name)) {
        $this->setLastName($name['familyName']);
      }
    }

    // Get the list of emails and a default email
    $token_email = null;
    $emails = $token->retriveEmails();
    if (count($emails)>0) {
      $token_email = $emails[0];
      $this->email =$token_email;

      // Add any unique new emails
      $emails = array_merge($emails, $this->emails);
      $flipped = array_flip($emails);
      // The emails are now keys, and duplicates have been removed
      $this->emails = array_keys($flipped);
    }

    // Add a new OpenIdentifier with data from the token
    $this->openIdentifier[] = new OpenIdentifier($openIdData, $token_email);
  }

  /**
   * Get an array of possible emails for this user
   * @return array $emails
   */
  public function getEmails()
  {
    return $this->emails;
  }

  /**
   * Set the openIdentifier collection
   *
   * @param \Doctrine\Common\Collections\ArrayCollection $openIdentifiers
   */
  public function setOpenIdentifier(ArrayCollection $openIdentifiers)
  {
    $this->openIdentifier = $openIdentifiers;
  }

  /**
   * Get the openIdentifier collection
   *
   * @return \Doctrine\Common\Collections\ArrayCollection
   */
  public function getOpenIdentifier()
  {
    return $this->openIdentifier;
  }

  public function serialize()
  {
    return serialize(
      array(
        $this->openIdentifier,
        $this->emails,
        parent::serialize()
      )
    );
  }

  public function unserialize($str)
  {
    list(
        $this->openIdentifier,
        $this->emails,
        $parentstr
      ) = unserialize($str);
    parent::unserialize($parentstr);
  }
}
