<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FSpires\CommitKeeperBundle\Entity\GroupCategory
 *
 * @ORM\Table(name="cgroup_category")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\GroupCategoryRepository")
 */
class GroupCategory extends Category
{
  /**
   * @var object $group
   *
   * @ORM\ManyToOne(targetEntity="Group", inversedBy="categories")
   * @ORM\JoinColumn(name="cgroup_id")
   */
  private $group;

  /**
   * @var integer order
   * The GroupCategories in the Group are sorted in this order
   * @ORM\Column(name="corder", type="integer", nullable=false)
   */
  private $order;


  /**
   * Set order
   *
   * @param integer $order
   */
  public function setOrder($order)
  {
    $this->order = $order;
  }

  /**
   * Get order
   *
   * @return integer 
   */
  public function getOrder()
  {
    return $this->order;
  }

  /**
   * Set group
   *
   * @param \FSpires\CommitKeeperBundle\Entity\Group $group
   */
  public function setGroup(Group $group)
  {
    $this->group = $group;
  }

  /**
   * Get group
   *
   * @return \FSpires\CommitKeeperBundle\Entity\Group
   */
  public function getGroup()
  {
    return $this->group;
  }

  public function addTag(Tag $tag)
  {
    throw new \LogicException('Cannot add tag to GroupCategory');
  }

  public function setTag(Tag $tag=null)
  {
    throw new \LogicException('Cannot set tag on GroupCategory');
  }
}
