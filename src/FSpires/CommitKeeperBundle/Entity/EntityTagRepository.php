<?php
namespace FSpires\CommitKeeperBundle\Entity;

use PDO;
use Doctrine\DBAL\Connection as DC;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use FSpires\CommitKeeperBundle\Entity\Tag;
use FSpires\CommitKeeperBundle\Entity\Category;
use FSpires\CommitKeeperBundle\Entity\GroupCategory;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * FSpires\CommitKeeperBundle\Entity\EntityTagRepository
 *
 */
class EntityTagRepository extends EntityRepository
{
  /**
   * @var array $tagCache
   * A cache of tags as they were when gotten from the database
   */
  private static $tagCache = array();


  protected function setEntEmptyTagCache($entity_name, $entity_id)
  {
    self::$tagCache[$entity_name . $entity_id] = array();
  }

  /**
   * Returns a collection of categories with the category ids as keys.
   * Each category has a sub-collection of tags.
   * If a group_id is given, the categories of the group comes first
   * and in the same order as in the group.
   *
   * @param string  $entity_name Database name of the entity_tag table
   * @param integer $entity_id   The id of the entity the tags belong to
   * @param integer $group_id    Optional, the id of the user Group
   * @param integer $category_id Optional, find only for one category
   * @return ArrayCollection of categories with tags
   */
  protected function findEntCategories($entity_name, $entity_id,
                                       $group_id=null, $category_id=null)
  {
    $param_values = array();
    $param_types  = array();

    $hasSource = false;
    $sql = 'SELECT ';
    if ($entity_name=='user' && $entity_id
        && $group_id && $category_id) {
      $hasSource = true;
      $sql .= 'DISTINCT ';
    }

    $isGroup = false;
    if ($entity_name=='cgroup') {
      $isGroup = true;
      $group_id = true;
    }

    $joinTags = (boolean)($entity_id && !$isGroup);

    $sql .= 'c.id AS category_id, c.name AS category';
    if ($joinTags) {
      $sql .= ', t.id AS id, t.name AS name';
      if ($hasSource) {
        $sql .= ", CASE WHEN ot.tag_id IS NOT NULL THEN 'O'"
          .           ' ELSE 0 END AS src';
      }
    } else {
      $sql .= ', NULL AS id, NULL AS name';
      if ($isGroup) {
        $sql .= ', gc.corder';
      }
    }

    $sql .= ' FROM category AS c';

    if ($isGroup || ($group_id && !$category_id)) {
      $sql .= ' INNER JOIN cgroup_category AS gc'
        . ' ON gc.category_id=c.id'
        . ' AND gc.cgroup_id=';
      if ($isGroup) {
        $sql .= ':entity_id';
        $param_values['entity_id'] = $entity_id;
        $param_types['entity_id']  = PDO::PARAM_INT;
      } else {
        $sql .= ':group_id';
        $param_values['group_id'] = $group_id;
        $param_types['group_id']  = PDO::PARAM_INT;
      }
    }

    if ($joinTags) {
      if ($group_id || $category_id) {
        $sql .= ' LEFT';
      } else {
        $sql .= ' INNER';
      }
      $sql .= ' JOIN (tag AS t'
        .  " INNER JOIN ${entity_name}_tag AS et ON et.tag_id=t.id"
        .  " AND et.${entity_name}_id=:entity_id)"
        . ' ON t.category_id=c.id';
      $param_values['entity_id'] = $entity_id;
      $param_types['entity_id']  = PDO::PARAM_INT;

      if ($hasSource) {
        $sql .= ' LEFT JOIN user_tag AS ot'
          . ' ON ot.tag_id=t.id'
          . ' AND ot.user_id!=:entity_id';
      }
    }
    if ($category_id) {
      $sql .= ' WHERE c.id=:category_id ORDER BY name';
      $param_values['category_id'] = $category_id;
      $param_types['category_id']  = PDO::PARAM_INT;
    } else {
      if ($group_id) {
        // We depend on FALSE being sorted before TRUE
        // $sql .= ' ORDER BY gc.corder IS NULL ASC, gc.corder ASC, ';
        $sql .= ' ORDER BY gc.corder ASC, ';
      } else {
        $sql .= ' ORDER BY ';
      }
      $sql .= 'c.name ASC, name ASC';
    }
    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    $categories = array();
    $ordered_categories = array();
    $old_tags = array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $category_id = $row['category_id'];
      if (!array_key_exists($category_id, $categories)) {
        if ($isGroup) {
          $category_obj = new GroupCategory($category_id);
          $category_obj->setOrder($row['corder']);
        } else {
          $category_obj = new Category($category_id);
        }
        $category_obj->setName($row['category']);
        $categories[$category_id] = $category_obj;
        $ordered_categories[] = $category_obj;
      } else {
        $category_obj = $categories[$category_id];
      }
      $id = $row['id'];
      if ($id) {
        $name = $row['name'];
        $old_tags[$id] = $name;
        $tag = new Tag($id);
        $tag->setName($name);
        if ($hasSource) {
          $tag->setSource($row['src']);
        }
        $category_obj->addTag($tag);
      }
    }
    self::$tagCache[$entity_name . $entity_id] = $old_tags;
    return new ArrayCollection($ordered_categories);
  }

  /**
   * Save tags into the database
   * Called by: GroupRepository::saveTags()
   *            UserRepository::saveTags()
   *            RequestRepository::saveTags()
   *
   * @param string $entity_name Name of entity connected to the tags
   * @param integer $entity_id Id of entity
   * @param array $categories Categories with modified tags to be
   *                              saved in DB
   * @param boolean $can_modify Can modify and add tags
   * @param boolean $do_purge Do a purge of orphan tags in DB
   * @throws \Doctrine\DBAL\ConnectionException
   * @throws \LogicException
   * @throws ValidatorException
   */
  protected function saveEntTags($entity_name, $entity_id, $categories,
                           $can_modify=true, $do_purge=true)
  {
    $cache_key = $entity_name . $entity_id;
    if (!array_key_exists($cache_key, self::$tagCache)) {
      $cache_key = $entity_name; // Try without id
      if (!array_key_exists($cache_key, self::$tagCache)) {
        throw new \LogicException("findCategories('$entity_name', $entity_id) must be called before saveTags()");
      }
    }
    $old_tags = self::$tagCache[$cache_key];

    $conn = $this->getEntityManager()->getConnection();
    $transStarted = false;
    try {
      // We loop through all the categories and tags
      foreach($categories as $category) {
        foreach($category->getTags() as $tag) {
          $tag_id = $tag->getId();
          if ($tag_id && array_key_exists($tag_id, $old_tags)) {
            // Tag exists
            if ($can_modify && !$tag->getSource()) {
              // Check if it is modified
              $name = trim($tag->getName());
              if (empty($name)) {
                throw new ValidatorException('category.tag.noEmpty');
              }
              if ($name!=$old_tags[$tag_id]) {
                // It is modified
                $do_modify = true;
                if (!$transStarted) {
                  $conn->beginTransaction();
                  $transStarted = true;
                }
                if ($entity_name=='user') {
                  // Only modify a tag if it belongs only to this user_id
                  $countSql = 'SELECT COUNT(tag_id) FROM user_tag'
                    . ' WHERE tag_id=:tag_id AND user_id!=:user_id';
                    /* 'SELECT COUNT(tag_id) FROM ('
                    . '(SELECT tag_id FROM cgroup_tag WHERE tag_id=:tag_id)'
                    .  'UNION ALL (SELECT tag_id FROM user_tag'
                    . ' WHERE tag_id=:tag_id AND user_id!=:user_id)'
                    . ') AS u';*/
                  $stmt = $conn->executeQuery(
                                    $countSql,
                                    array('tag_id'  => $tag_id,
                                          'user_id' => $entity_id),
                                    array('tag_id'  => PDO::PARAM_INT,
                                          'user_id' => PDO::PARAM_INT)
                                              );
                  $count = $stmt->fetchColumn();
                  if ($count>0) {
                    $do_modify = false;
                  }
                }
                if ($do_modify) {
                  $updateSql = 'UPDATE tag SET name=:name WHERE id=:tag_id';
                  $conn->executeUpdate($updateSql,
                                       array('name'   => $name,
                                             'tag_id' => $tag_id),
                                       array('name'   => PDO::PARAM_STR,
                                             'tag_id' => PDO::PARAM_INT)
                                       );
                }
              }
            }
            unset($old_tags[$tag_id]);
          } else {
            // It is a new tag
            if (!$transStarted) {
              $conn->beginTransaction();
              $transStarted = true;
            }
            if (!$tag_id) {
              if (!$can_modify) {
                throw new \LogicException(
                  'Tags must already exist in the DB and have an ID');
              }
              $name = trim($tag->getName());
              if (empty($name)) {
                // Empty name means it is a new tag we do not add
                continue;
              }
              $category_id = $category->getId();
              // Check if it exists for some other user or group
              $selectSql = 'SELECT id FROM tag'
                . ' WHERE category_id=:category_id'
                . ' AND name=:name';
              $stmt = $conn->executeQuery(
                                $selectSql,
                                array('category_id' => $category_id,
                                      'name'        => $name),
                                array('category_id' => PDO::PARAM_INT,
                                      'name'        => PDO::PARAM_STR)
                                          );
              $tag_id = $stmt->fetchColumn();
              if (!$tag_id) {
                $insertSql = 'INSERT INTO tag (category_id, name)'
                  . ' VALUES (:category_id, :name)';
                $conn->executeUpdate(
                          $insertSql,
                          array('category_id' => $category_id,
                                'name'        => $name),
                          array('category_id' => PDO::PARAM_INT,
                                'name'        => PDO::PARAM_STR)
                                     );
                $tag_id = $conn->lastInsertId();
                if (!$tag_id) {
                  throw new \LogicException(
                    'Tag id should have been set at this point');
                }
              }
              $tag->setId($tag_id);
            }
            // Got a new tag ready to be inserted
            $insertSql = "INSERT INTO ${entity_name}_tag"
              . " (${entity_name}_id, tag_id)"
              . ' VALUES (:entity_id, :tag_id)';
            $conn->executeUpdate(
                      $insertSql,
                      array('entity_id' => $entity_id,
                            'tag_id'    => $tag_id),
                      array('entity_id' => PDO::PARAM_INT,
                            'tag_id'    => PDO::PARAM_INT)
                                 );
          }
        }
      }
      // Delete tags that are no longer in the collection
      if (count($old_tags)>0) {
        $tagsToDelete = array_keys($old_tags);
        $deleteSql = "DELETE FROM ${entity_name}_tag"
          . " WHERE ${entity_name}_id=:entity_id AND tag_id IN (:tag_ids)";
        $conn->executeUpdate($deleteSql,
                             array('entity_id' => $entity_id,
                                   'tag_ids'   => $tagsToDelete),
                             array('entity_id' => PDO::PARAM_INT,
                                   'tag_ids'   => DC::PARAM_INT_ARRAY)
                             );
        if ($do_purge) {
          $purgeSql = 'DELETE FROM t USING tag AS t'
            . ' LEFT JOIN user_tag AS ut ON ut.tag_id=t.id'
            . ' LEFT JOIN request_tag AS rt ON rt.tag_id=t.id'
            . ' WHERE ut.tag_id IS NULL'
            .  ' AND rt.tag_id IS NULL';
          $conn->executeUpdate($purgeSql);
        }
      }
      if ($transStarted) {
        $conn->commit();
      }
    } catch (\Exception $e) {
      if ($transStarted) {
        $conn->rollback();
      }
      throw $e;
    }
  }
}
