<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FSpires\CommitKeeperBundle\Entity\UserBase;
use FSpires\CommitKeeperBundle\Entity\TrafficLight;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Model\Enum\Phase;

/**
 * FSpires\CommitKeeperBundle\Entity\History
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\HistoryRepository")
 */
class History
{
  const ShortDescLength = 240;

  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var datetime $eventDate
   *
   * @ORM\Column(name="event_date", type="datetime", nullable=false)
   */
  private $eventDate;

  /**
   * @var integer $revision
   *
   * @ORM\Column(name="revision", type="integer", nullable=false)
   */
  private $revision;

  /**
   * @var object $request
   *
   * @ORM\ManyToOne(targetEntity="Request")
   * @ORM\JoinColumn(name="request_id", referencedColumnName="id")
   */
  private $request;

  /**
   * @var \FSpires\CommitKeeperBundle\Entity\UserName $actor
   *
   * @ORM\ManyToOne(targetEntity="User")
   * @ORM\JoinColumn(name="actor_id", referencedColumnName="id")
   */
  private $actor;

  /**
   * @var string $role
   *
   * @ORM\Column(name="role", type="string", length=1, nullable=false)
   */
  private $role;

  /**
   * @var string $actionStr
   *
   * @ORM\Column(name="action", type="string", length=25, nullable=false)
   */
  private $actionStr;

  /**
   * @var string $phase
   *
   * @ORM\Column(name="phase", type="string", length=20, nullable=false)
   */
  private $phase;

  /**
   * @var boolean $phaseChanged
   *
   * @ORM\Column(name="phase_changed", type="boolean", nullable=false)
   */
  private $phaseChanged;

  /**
   * @var TrafficLight $trafficLight
   *
   * Icon to display what state the request is in
   *
   * @ORM\ManyToOne(targetEntity="TrafficLight")
   * @ORM\JoinColumn(name="traffic_light", referencedColumnName="id")
   */
  private $trafficLight;

  /**
   * @var boolean $trafficLightChanged
   *
   * @ORM\Column(name="traffic_light_changed", type="boolean", nullable=false)
   */
  private $trafficLightChanged;

  /**
   * @var \DateTime $dueDate
   *
   * @ORM\Column(name="due_date", type="date", nullable=false)
   */
  private $dueDate;

  /**
   * @var \DateTime $dueDatePrevious
   *
   * @ORM\Column(name="due_date_previous", type="date", nullable=true)
   */
  private $dueDatePrevious;

  /**
   * @var string $budget
   *
   * @ORM\Column(name="budget", type="string", length=25, nullable=true)
   */
  private $budget;

  /**
   * @var string $budgetPrevious
   *
   * @ORM\Column(name="budget_previous", type="string", length=25, nullable=true)
   */
  private $budgetPrevious;

  /**
   * @var string $description
   *
   * @ORM\Column(name="description", type="text", nullable=false)
   */
  private $description;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set revision
   *
   * @param integer $revision
   */
  public function setRevision($revision)
  {
    $this->revision = $revision;
  }

  /**
   * Get revision
   *
   * @return integer
   */
  public function getRevision()
  {
    return $this->revision;
  }

  /**
   * Set some of the data in this history object
   * from the provided Request object
   * @param Request $req
   */
  public function setFromRequest(Request $req)
  {
    $this->request = $req;
    $this->phase = $req->getPhase();
    $this->trafficLight = $req->getTrafficLight();
    $this->dueDate = $req->getDueDate();
    $this->budget = $req->getBudget();
    $this->description = $req->getDescription();
  }

  /**
   * Get eventDate
   *
   * @return datetime 
   */
  public function getEventDate()
  {
    return $this->eventDate;
  }

  /**
   * Set request
   *
   * @param \FSpires\CommitKeeperBundle\Entity\Request $request
   */
  public function setRequest(Request $request)
  {
    $this->request = $request;
  }

  /**
   * Get request
   *
   * @return \FSpires\CommitKeeperBundle\Entity\Request
   */
  public function getRequest()
  {
    return $this->request;
  }

  /**
   * Set role
   *
   * @param string $role
   */
  public function setRole($role)
  {
    $this->role = $role;
  }

  /**
   * Get role
   *
   * @return string 
   */
  public function getRole()
  {
    return $this->role;
  }

  /**
   * Set actionStr
   *
   * @param string $actionStr
   */
  public function setActionStr($actionStr)
  {
    $this->actionStr = $actionStr;
  }

  /**
   * Get actionStr
   *
   * @return string 
   */
  public function getActionStr()
  {
    return $this->actionStr;
  }

  /**
   * Set phase
   *
   * @param string $phase
   */
  public function setPhase($phase)
  {
    $this->phase = $phase;
  }

  /**
   * Get phase
   *
   * @return string 
   */
  public function getPhase()
  {
    return $this->phase;
  }

  /**
   * Set phaseChanged
   *
   * @param boolean $phaseChanged
   */
  public function setPhaseChanged($phaseChanged)
  {
    $this->phaseChanged = (boolean)$phaseChanged;
  }

  /**
   * Has phase changed?
   *
   * @return boolean 
   */
  public function isPhaseChanged()
  {
    return $this->phaseChanged;
  }

  /**
   * Set trafficLightChanged
   *
   * @param boolean $trafficLightChanged
   */
  public function setTrafficLightChanged($trafficLightChanged)
  {
    $this->trafficLightChanged = (boolean)$trafficLightChanged;
  }

  /**
   * Has Traffic Light changed?
   *
   * @return boolean 
   */
  public function isTrafficLightChanged()
  {
    return $this->trafficLightChanged;
  }

  /**
   * Set dueDate
   *
   * @param \DateTime $dueDate
   */
  public function setDueDate(\DateTime $dueDate)
  {
    $this->dueDate = $dueDate;
  }

  /**
   * Get dueDate
   *
   * @return \DateTime
   */
  public function getDueDate()
  {
    return $this->dueDate;
  }

  /**
   * Set previous due date
   *
   * @param \DateTime $dueDatePrevious
   */
  public function setDueDatePrevious($dueDatePrevious)
  {
    $this->dueDatePrevious = $dueDatePrevious;
  }

  /**
   * Get previous due date
   *
   * @return \DateTime
   */
  public function getDueDatePrevious()
  {
    return $this->dueDatePrevious;
  }

  /**
   * Set budget
   *
   * @param string $budget
   */
  public function setBudget($budget)
  {
    $this->budget = $budget;
  }

  /**
   * Get budget
   *
   * @return string
   */
  public function getBudget()
  {
    return $this->budget;
  }

  /**
   * Set the previous budget
   *
   * @param string $budgetPrevious
   */
  public function setBudgetPrevious($budgetPrevious)
  {
    $this->budgetPrevious = $budgetPrevious;
  }

  /**
   * Get the previous budget
   *
   * @return string
   */
  public function getBudgetPrevious()
  {
    return $this->budgetPrevious;
  }

  public function getNeedsTranslation()
  {
    return 'S' === $this->role;
  }

  /**
   * Set description
   *
   * @param text $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }

  /**
   * Get description
   *
   * @return text/html
   */
  public function getDescription()
  {
    $desc = htmlentities($this->description, ENT_NOQUOTES|ENT_IGNORE, 'UTF-8');
    $desc = str_replace("\r",'',$desc);
    $desc = str_replace("\n\n",'<p>',$desc);
    return str_replace("\n","<br>\n", $desc);
  }

  /**
   * Get short description
   *
   * @return text 
   */
  public function getShortDesc()
  {
    $maxLen = self::ShortDescLength;
    $desc = $this->description;
    if (strlen($desc)>$maxLen) {
      $desc = substr($desc, 0, $maxLen) . '...';
    }
    return $desc;
  }

  /**
   * Get content of extra history line(s), if any
   *
   * @param string $dateFormat Format of dates
   * @return array of text lines
   */
  public function getExtra($dateFormat)
  {
    $extra = array();
    if (isset($this->dueDatePrevious)
        && $this->dueDate != $this->dueDatePrevious) {
      $extra[] =
        array('msg'=>'DueDateChanged%fromDate%_%toDate%',
              'vars'=>array(
                 '%fromDate%'=>$this->dueDatePrevious->format($dateFormat),
                 '%toDate%'=>$this->dueDate->format($dateFormat)
                            )
              );
    }

    if (isset($this->budgetPrevious) &&
        ($this->budget != $this->budgetPrevious)) {
      $extra[] = array('msg'=>'BudgetChanged%fromBudget%_%toBudget%',
                       'vars'=>array('%fromBudget%'=>$this->budgetPrevious,
                                     '%toBudget%'=>$this->budget));
    } else if ((!isset($this->budgetPrevious)) && $this->budget
               && substr($this->actionStr,0,4)!='Make') {
      $extra[] = array('msg'=>'BudgetChanged%toBudget%',
                       'vars'=>array('%toBudget%'=>$this->budget));
    }

    if ($this->trafficLightChanged && !$this->phaseChanged
        && Phase::Delivery==$this->phase) {
      $extra[] = array('msg'=>$this->trafficLight->getStatusMsg(),
                       'vars'=>array());
    }

    return $extra;
  }

  /**
   * Set actor
   *
   * @param \FSpires\CommitKeeperBundle\Entity\User $actor
   */
  public function setActor(User $actor)
  {
    $this->actor = $actor;
  }

  /**
   * Get actor
   *
   * @return \FSpires\CommitKeeperBundle\Entity\User
   */
  public function getActor()
  {
    return $this->actor;
  }

  /**
   * Set trafficLight
   *
   * @param \FSpires\CommitKeeperBundle\Entity\TrafficLight $trafficLight
   */
  public function setTrafficLight(TrafficLight $trafficLight)
  {
    $this->trafficLight = $trafficLight;
  }

  /**
   * Get trafficLight
   *
   * @return \FSpires\CommitKeeperBundle\Entity\TrafficLight
   */
  public function getTrafficLight()
  {
    return $this->trafficLight;
  }
}
