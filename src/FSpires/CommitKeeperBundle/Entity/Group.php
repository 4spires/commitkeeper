<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use FSpires\CommitKeeperBundle\Entity\UserBase;

/**
 * FSpires\CommitKeeperBundle\Entity\Group
 *
 * @Assert\Callback(methods={"validateCategories"})
 *
 * @ORM\Table(name="cgroup")
 * @ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\GroupRepository")
 */
class Group
{
  /**
   * @var integer $id
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue
   */
  private $id;

  /**
   * @var string $name
   *
   * @ORM\Column(name="name", type="string", length=50, nullable=false)
   */
  private $name;

  /**
   * @var boolean $has_budget_field
   *
   * @ORM\Column(name="has_budgetf", type="boolean", nullable=false)
   */
  private $hasBudgetField;

  /**
   * @var string $budgetUnit;
   * @ORM\ManyToOne(targetEntity="BudgetUnit", fetch="EAGER")
   * @ORM\JoinColumn(name="budgetf_unit_id", referencedColumnName="id")
   */
  private $budgetUnit;

  /**
   * @var Doctrine\Common\Collections\ArrayCollection $categories;
   *
   * Categories and tags are handeled manually by our own code
   * This ORM relationship is not used
   * (ORM\OneToMany(targetEntity="GroupCategory", mappedBy="group"))
   * (ORM\OrderBy({"order"="ASC"}))
   */
  private $categories;

  /**
   * @var array $catCache
   * A cache of categories as they were when gotten from the database
   */
  private $catCache;

  /**
   * @var EntityManagerInterface $em Doctrine\ORM\Entitymanager
   * Not saved in the database
   */
  private $em;


  public function __construct()
  {
    $this->categories = new ArrayCollection();
  }

  /**
   * Get Id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set name
   *
   * @param string $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * Get name
   *
   * @return string 
   */
  public function getName()
  {
    return $this->name;
  }

  public function setNameFromUser(UserBase $user)
  {
    $gname = $user->getName() . '<' . $user->getEmail() . '>';
    $this->name = substr($gname, 0, 200);
  }

  /**
   * Set Entitymanager
   *
   * @param EntityManagerInterface $em Doctrine\ORM\Entitymanager
   */
  public function setEntityManager(EntityManagerInterface $em)
  {
    $this->em = $em;
  }

  /**
   * Get Entitymanager
   *
   * @return EntityManagerInterface Doctrine\ORM\Entitymanager
   */
  public function getEntityManager()
  {
    if (!$this->em) {
      throw new \RuntimeException('EntityManager is not set in the Group object');
    }
    return $this->em;
  }

  /**
   * Add a category object to the collection of categories
   *
   * @param FSpires\CommitKeeperBundle\Entity\GroupCategory $categories
   */
  public function addGroupCategory(GroupCategory $gcategory)
  {
    $gcategory->setGroup($this);

    if (!$gcategory->getOrder()) {
      $order = 1;
      $count = count($this->categories);
      if ($count>0) {
        $lastCatOrder = $this->categories->last()->getOrder();
        if ($lastCatOrder>$count) {
          $order = $lastCatOrder + 1;
        } else {
          $order = $count + 1;
        }
      }
      $gcategory->setOrder($order);
    }
    $this->categories[] = $gcategory;
  }

  /**
   * Create and add a category by just giving the name
   *
   * @param string $name
   * @param Doctrine\ORM\EntityManager $em Doctrine Entity Manager
   */
  public function addCategory($name)
  {
    $gcategory = new GroupCategory();
    $gcategory->setName($name);
    $this->addGroupCategory($gcategory);
  }

  /**
   * Get categories
   *
   * @return Doctrine\Common\Collections\Collection 
   */
  public function getCategories(EntityManagerInterface $em=null)
  {
    if ($this->categories) {
      return $this->categories;
    }
    if (!$this->id) {
      throw new \LogicException('No group id found. The Group must be saved in the database before its categories can be accessed');
    }
    if ($em) {
      $this->em = $em;
    } else {
      $em = $this->getEntityManager();
    }
    $repo = $em->getRepository('CKBundle:Group');
    $this->categories = $repo->findCategories($this->id);
    $this->catCache = array();
    foreach ($this->categories as $gcategory) {
      $gcategory->setGroup($this);
      $this->catCache[$gcategory->getId()] = clone $gcategory;
    }
    return $this->categories;
  }

  /**
   * Save this groups categories
   */
  public function saveCategories()
  {
    if (!$this->id) {
      throw new \LogicException('No Group id. The group must be saved in the database before the categories can be saved.');
    }
    if (!is_array($this->catCache)) {
      throw new \LogicException("getCategories() must be called before saveCategories()");
    }
    $em = $this->getEntityManager();
    $repo = $em->getRepository('CKBundle:Group');
    $repo->saveCategories($this->id, $this->categories, $this->catCache);
    $this->catCache = null;
    $this->categories = null;
  }

  /**
   * Validate the categories
   * Check that not two of them is alike
   */
  public function validateCategories($ec) {
    $Names = array();
    foreach($this->categories as $cat) {
      $name = trim($cat->getName());
      if ($name) {
        if (array_key_exists($name, $Names)) {
          $ec->addViolationAt('categories', 'category.noSame%name%', array('%name%' => $name), $this->categories);
        }
        $Names[$name] = 1;
      }
    }
  }

  /**
   * Save tags in this groups categories
   */
  public function saveTags()
  {
    if (!$this->id) {
      throw new \LogicException('No Group id. The group must be saved in the database before the tags can be saved.');
    }
    $em = $this->getEntityManager();
    $repo = $em->getRepository('CKBundle:Group');
    $repo->saveTags($this->id, $this->categories);
  }

  /**
   * Add some default categories for a new user
   */
  public function addDefaultCategories($from_user_id, EntityManagerInterface $em=null)
  {
    if (!$this->id) {
      throw new \LogicException('No Group id. The group must be saved in the database before the default categories can be added.');
    }
    if ($em) {
      $this->em = $em;
    } else {
      $em = $this->getEntityManager();
    }
    $repo = $em->getRepository('CKBundle:Group');
    $repo->addDefaultCategories($this->id, $from_user_id);
  }

  /**
   * Set hasBudgetField
   *
   * @param boolean $hasBudgetField
   * @return Group
   */
  public function setHasBudgetField($hasBudgetField)
  {
    $this->hasBudgetField = (boolean)$hasBudgetField;
  }

  /**
   * Get hasBudgetField
   * The Symfony2 Form requires this ugly getter
   *
   * @return boolean 
   */
  public function getHasBudgetField()
  {
    return $this->hasBudgetField;
  }

  /**
   * hasBudgetField
   *
   * @return boolean 
   */
  public function hasBudgetField()
  {
    return $this->hasBudgetField;
  }

  /**
   * Set budgetUnit
   *
   * @param BudgetUnit $budgetUnit
   */
  public function setBudgetUnit(BudgetUnit $budgetUnit)
  {
    $this->budgetUnit = $budgetUnit;
  }

  /**
   * Get budgetUnit
   *
   * @return BudgetUnit 
   */
  public function getBudgetUnit()
  {
    return $this->budgetUnit;
  }
}
