<?php

namespace FSpires\CommitKeeperBundle\Entity;

/**
 * A TimeZone
 *
 * ORM\Entity(repositoryClass="FSpires\CommitKeeperBundle\Entity\TimeZoneRepository")
 */
class TimeZone
{
  /**
   * Offset from GMT in hours
   * @var float
   */
  private $hoursOffset;

  /**
   * Human readable label
   *
   * @var string
   */
  private $label;

  /**
   * PHP zone name
   * One zone that is representative for this GMT offset.
   *
   * @var string
   */
  private $zoneName;

  /**
   * The time slot used when sending emails etc.
   * This is generally the same as the time zone hour offset,
   * but has only whole numbers between -11 and +12
   *
   * @var int
   */
  private $slot;

  /**
   * @var TimeZoneRepository
   */
  private static $repository;

  /**
   * Get Repository
   *
   * @return TimeZoneRepository
   */
  public static function getRepository()
  {
    if (empty(self::$repository)) {
      self::$repository = new TimeZoneRepository();
    }
    return self::$repository;
  }

  /**
   * Create a new TimeZone instance based on the minutes offset from GMT given.
   *
   * @param $minutesOffset
   * @return TimeZone|null
   */
  public static function createFromOffset($minutesOffset)
  {
    return self::getRepository()->findByOffsetInMinutes($minutesOffset);
  }

  /**
   * @param string $label
   * @param string $sampleZoneName
   * @param float $hoursOffset
   * @param $slot
   */
  public function __construct($label, $sampleZoneName, $hoursOffset, $slot)
  {
    $this->hoursOffset = $hoursOffset;
    $this->label = $label;
    $this->zoneName = $sampleZoneName;
    $this->slot = $slot;
  }

  /**
   * @return int
   */
  public function getOffsetInMinutes()
  {
    return intval(60*$this->hoursOffset);
  }

  /**
   * @return float
   */
  public function getOffsetInHours()
  {
    return $this->hoursOffset;
  }

  /**
   * @return string
   */
  public function getLabel()
  {
    return $this->label;
  }

  /**
   * @return int
   */
  public function getSlot()
  {
    return $this->slot;
  }

  /**
   * @return string
   */
  public function getZoneName()
  {
    return $this->zoneName;
  }

  /**
   * @return \DateTimeZone
   */
  public function getDateTimeZone()
  {
    return new \DateTimeZone($this->zoneName);
  }
}
