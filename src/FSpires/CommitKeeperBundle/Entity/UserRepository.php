<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FSpires\CommitKeeperBundle\Entity\EntityTagRepository;
use FSpires\CommitKeeperBundle\Entity\Tag;
use Doctrine\DBAL\Types\Type as DocType;
use PDO, DateTime;

/**
 * UserRepository
 */
class UserRepository extends EntityTagRepository
{
  /**
   * Find users that are new (userType=1)
   * @param array $ids Only search among users with ids in this array
   * @return array New users found
   */
  public function findNewUsers($ids, $maxType=1)
  {
    $newUsers = array();
    $searchIds = array();
    $unitOfWork = $this->getEntityManager()->getUnitOfWork();
    $ubUserType = $maxType+1;
    foreach ($ids as $id) {
      if ($user = $unitOfWork->tryGetById(array('id'=>$id),
                           'FSpires\CommitKeeperBundle\Entity\User')) {
        if ($user->getUserType() < $ubUserType) {
          $newUsers[] = $user;
        }
      } else {
        $searchIds[] = $id;
      }
    }

    if (count($searchIds) > 0) {
      $newUsers2 = $this->createQueryBuilder('u')
        ->where('u.userType<'.$ubUserType.' AND u.id IN (:ids)')
        ->setParameter('ids', $searchIds)
        ->getQuery()->getResult();
      if (count($newUsers2) > 0) {
        $newUsers = array_merge($newUsers, $newUsers2);
      }
    }

    return $newUsers;
  }

  /**
   * Find valid temporary passwords for a user
   */
  public function findTemporaryPasswords($user_id)
  {
    $sql ='SELECT id, password FROM temp_password'
      . ' WHERE user_id=:user_id AND used=0 AND expiry_date>:dnow'
      . ' ORDER BY created_date DESC';
    $param_values = array('user_id' => $user_id,
                          'dnow' => new DateTime());
    $param_types  = array('user_id' => PDO::PARAM_INT,
                          'dnow' => DocType::DATETIME);
    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    $passwords = array();
    if ($stmt) {
      while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
        $passwords[$row[0]] = $row[1];
      }
    }
    return $passwords;
  }

  /**
   * Flag that this temporary password has been used
   */
  public function usedTemporaryPassword($id, $date) {
    $sql = 'UPDATE temp_password SET used=1, expiry_date=:date WHERE id=:id';
    $param_values = array('date' => $date,
                          'id'   => $id);
    $param_types  = array('date' => DocType::DATETIME,
                          'id'   => PDO::PARAM_INT);
    $conn = $this->getEntityManager()->getConnection();
    $conn->executeUpdate($sql, $param_values, $param_types);
  }

  /**
   * Find tags that is in use for this category,
   * but that the user does not have.
   * Returns a collection of tags.
   *
   * @param integer $user_id     The id of the User
   * @param integer $category_id The id of the Category
   * @return ArrayCollection of tags
   */
  public function findUsedTags($user_id, $category_id)
  {
    $sql ='SELECT DISTINCT t.id, t.name FROM tag AS t'
      . ' LEFT JOIN user_tag AS ut ON ut.tag_id=t.id AND ut.user_id=:user_id'
      . ' LEFT JOIN user_tag AS ot ON ot.tag_id=t.id AND ot.user_id!=:user_id'
      . ' LEFT JOIN user_contact AS uc ON uc.user_id=:user_id AND uc.contact_id=ot.user_id'
      . ' WHERE ut.tag_id IS NULL AND uc.contact_id IS NOT NULL'
      .  ' AND t.category_id=:category_id'
      . ' ORDER BY t.name ASC';

    $param_values = array('user_id'     => $user_id,
                          'category_id' => $category_id);
    $param_types  = array('user_id'     => PDO::PARAM_INT,
                          'category_id' => PDO::PARAM_INT);
    $conn = $this->getEntityManager()->getConnection();
    $stmt = $conn->executeQuery($sql, $param_values, $param_types);
    $tags = array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $tag = new Tag($row['id']);
      $tag->setName($row['name']);
      $tags[] = $tag;
    }
    return new ArrayCollection($tags);
  }

  /**
   * Find all tags that belong to an user.
   *
   * Returns a collection of categories with the category ids as keys.
   * Each category has a sub-collection of tags.
   * If a group_id is given, the categories of the group comes first
   * and in the same order as in the group.
   *
   * @param integer $user_id     The id of the User
   * @param integer $group_id    Optional, the id of the users Group
   * @param integer $category_id Optional, find only for one category
   * @return ArrayCollection of categories with tags
   */
  public function findCategories($user_id, $group_id=null, $category_id=null)
  {
    return $this->findEntCategories('user', $user_id, $group_id, $category_id);
  }

  /**
   * Save tags that belong to an user.
   *
   * @param integer $user_id The id of the User
   * @param ArrayCollection $categories The categories with tags to save
   */
  public function saveTags($user_id, $categories)
  {
    $this->saveEntTags('user', $user_id, $categories);
  }

  /**
   * Find all users with a certain time zone slot (offset).
   * The time zone slot can only be a whole number of hours.
   * Returns all users with that time zone slot.
   *
   * @param int $timeZoneSlot
   * @return Collection
   */
  public function findByTimeZoneSlot($timeZoneSlot)
  {
    return $this->findBy(array(
      'timeZoneSlot' => $timeZoneSlot,
      'dailyReportEnabled' => true,
      'active' => true
    ));
  }
}
