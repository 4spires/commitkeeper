<?php
namespace FSpires\CommitKeeperBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ImageException extends \RuntimeException {}

/**
 * A profile picture
 *
 * @ORM\Table(name="picture")
 * @ORM\Entity
 * @Assert\Callback(methods={"validatePicture"})
 */
class Picture extends UploadedFile
{
  const ImageSize = 48;

  const MinFileSize = 80;
  const MaxFileSize = 128000000;

  /**
   * Picture belongs to this user
   * @var User user
   *
   * @ORM\OneToOne(targetEntity="User", inversedBy="picture")
   * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
   */
  private $user;

  /**
   * True if this is an uploaded file
   * @var bool $hasFile
   * @ORM\Column(name="has_file", type="boolean", nullable=false)
   */
  private $hasFile = false;

  /**
   * What to use if no gravatar is found
   * @var string $gravatarDefault
   * @ORM\Column(name="gravatar_default", type="string", length=10, nullable=false)
   */
  private $gravatarDefault = 'mm';

  /**
   * Temporary storage for a resized image
   */
  private $resizedImage = false;


  /**
   * True if this is a gravatar
   * @var bool $isGravatar
   */
  private $isGravatar;

  /**
   * Check that we have a valid picture before the form
   * can be submitted.
   */
  public function validatePicture($ec)
  {
    // This check can only be first if we have a checkbox for using
    // a gravatar or not. That checkbox is currently removed.
    /*
    if ($this->isGravatar) {
      $this->useGravatar();
      return;
    }
    */

    if (!parent::hasNewUploadedFile()) {
      if ($this->isGravatar) {
        $this->useGravatar();
      }
      return;
    }

    // Will also set up any old file to be removed
    $this->setHasFile(true);

    parent::setFileAttributes();
    if (strncasecmp('image/', $this->getContentType(), 6) != 0) {
      $ec->addViolationAt('file','picture.file.wrongContentType');
      return;
    }

    $fileSize = $this->file->getSize();
    if ($fileSize < self::MinFileSize) {
      $ec->addViolationAt('file', 'picture.file.tooSmall%fileSize%', array('%fileSize%' => $fileSize));
      return;
    }
    if ($fileSize > self::MaxFileSize) {
      $ec->addViolationAt('file','picture.file.tooLarge');
      return;
    }

    try {
      $this->resizeImage();
    } catch (\Exception $ex) {
      $ec->addViolationAt('file', $ex->getMessage());
    }
  }

  /**
   * Resize the uploaded image if necessary
   */
  protected function resizeImage() {
    $filename = $this->getFile()->getRealPath();
    if (!$filename) {
      throw new ImageException('picture.file.notFound');
    }
    $info = @GetImageSize($filename);
    if (!$info || $info[0] < 2 || $info[1] < 2) {
      throw new ImageException('picture.file.invalid');
    }
    $srcW = $info[0];
    $srcH = $info[1];
    $type = $info[2];
    $mimeType = $info['mime'];

    $dstW = self::ImageSize;
    $dstH = self::ImageSize;

    $doConvert = true;
    $this->resizedImage = false;

    if ($srcW==$dstW && $srcH==$dstH) {
      switch($type) {
      case IMAGETYPE_GIF:
      case IMAGETYPE_JPEG:
      case IMAGETYPE_PNG:
        $doConvert = false;
        break;
      default:
      }
    }

    if ($doConvert) {
      $image = false;
      switch($type) {
      case IMAGETYPE_GIF:
        $image = ImageCreateFromGIF($filename);
        break;
      case IMAGETYPE_JPEG:
        $image = ImageCreateFromJPEG($filename);
        break;
      case IMAGETYPE_PNG:
        $image = ImageCreateFromPNG($filename);
        break;
      case IMAGETYPE_XBM:
        $image = ImageCreateFromXBM($filename);
        break;
      case IMAGETYPE_WBMP:
        $image = ImageCreateFromWBMP($filename);
        break;
      default:
        throw new ImageException('picture.file.wrongType');
      }

      if (!$image) {
        throw new ImageException('picture.file.readFail');
      }

      $new_image = ImageCreateTrueColor($dstW, $dstH);
      ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $dstW, $dstH, $srcW, $srcH);
      ImageDestroy($image);

      $this->resizedImage = $new_image;

      $this->url = '.jpeg';
      $mimeType = 'image/jpeg';
    }

    $this->setContentType($mimeType);
  }

  /**
   * Check if we have an uploaded file
   */
  protected function hasNewUploadedFile()
  {
    if ($this->isGravatar) return false;
    return parent::hasNewUploadedFile();
  }

  /**
   * Set user
   *
   * @param \FSpires\CommitKeeperBundle\Entity\User $user
   */
  public function setUser(User $user = null)
  {
    $this->user = $user;
    if ($user && $user->getPicture()!=$this) {
      $user->setPicture($this);
    }
  }

  /**
   * Get user
   *
   * @return \FSpires\CommitKeeperBundle\Entity\User 
   */
  public function getUser()
  {
    return $this->user;
  }

  /**
   * Override parent method
   */
  public function setFileAttributes()
  {
    // This is done in the validatePicture method instead
  }

  /**
   * We inherit post persist for this method
   */
  public function storeFile()
  {
    // If $this->filenameForRemove is set, remove the old file
    $this->removeFile();

    // Then check if we have a new file
    if (!$this->hasNewUploadedFile()) return;

    if ($this->resizedImage) {
      $filename = $this->getAbsoluteFilePath();
      ImageJPEG($this->resizedImage, $filename, 100);
      ImageDestroy($this->resizedImage);

      // Remove the uploaded file
      unlink($this->file->getRealPath());
    } else {
      $this->file->move($this->getUploadDir(), $this->getFileName());
    }

    // Make sure only newly uploaded files are stored
    unset($this->file);
  }

  /**
   * Get the file name of the file, with the absolute file path
   */
  public function getAbsoluteFilePath()
  {
    if ($this->hasFile) {
      return parent::getAbsoluteFilePath();
    }
    return false;
  }

  /**
   * Get the absolute directory path to the directory where the uploaded
   * files and documents are stored.
   */
  protected function getUploadDir()
  {
    return __DIR__.'/../../../../app/pictures';
  }

  /**
   * Get the URL used when the picture is in a file
   */
  private function getUrlForFile()
  {
    return '/picture/'. $this->getId();
  }

  /**
   * Get the computed Url for this picture
   *
   * @return string 
   */
  public function getUrl()
  {
    if ($this->hasFile) {
      return $this->getUrlForFile();
    }

    return $this->url;
  }
 
  /**
   * Set an image URL to be used as picture
   */
  public function setUrl($url)
  {
    $this->setHasFile(false);
    $this->url = $url;
    $this->setContentType('');
    $this->setUpdatedDate();
  }

  /**
   * Use a gravatar for the picture
   * Set the gravatar email to be that of the associated user
   */
  public function useGravatar()
  {
    $this->setUrl($this->getGravatarUrl());
  }

  /**
   * Get URL for a gravatar to be used as picture
   *
   * @param string|null $default
   * @throws \LogicException
   * @return string
   */
  public function getGravatarUrl($default=null)
  {
    if (!$this->user) {
      throw new \LogicException('The user is not set in the picture object');
    }
    if (is_null($default)) {
      $default = $this->gravatarDefault;
    }
    $hash = md5(strtolower(trim($this->user->getEmail())));
    $url = 'http://www.gravatar.com/avatar/' . $hash . '?s=' . self::ImageSize;
    if ($default) {
      $url .= '&d=' . $default;
    }
    return $url;
  }

  /**
   * Set hasFile
   *
   * @param boolean $hasFile
   */
  protected function setHasFile($hasFile)
  {
    if ($this->hasFile) {
      $this->storeFilenameForRemove();
    }

    $this->hasFile = (boolean)$hasFile;
    $this->isGravatar = !$this->hasFile;
  }

  /**
   * Get hasFile
   *
   * @return boolean 
   */
  public function getHasFile()
  {
    return $this->hasFile;
  }

  /**
   * Set isGravatar
   *
   * @param boolean $isGravatar
   */
  public function setIsGravatar($isGravatar)
  {
    $this->isGravatar = (boolean)$isGravatar;
  }

  /**
   * Get isGravatar
   *
   * @return boolean
   */
  public function getIsGravatar()
  {
    return $this->isGravatar;
  }

  /**
   * Is this a gravatar?
   *
   * @return boolean
   */
  public function isGravatar()
  {
    return $this->isGravatar;
  }

  /**
   * Set gravatarDefault
   *
   * @param string $gravatarDefault
   */
  public function setGravatarDefault($gravatarDefault)
  {
    $this->gravatarDefault = $gravatarDefault;
  }

  /**
   * Get gravatarDefault
   *
   * @return string 
   */
  public function getGravatarDefault()
  {
    return $this->gravatarDefault;
  }

  /**
   * Get a list of choices for a default gravatar
   */
  public static function getDefaultGravatarChoices()
  {
    return array('identicon' => 'picture.gravatar.identicon',
                 'monsterid' => 'picture.gravatar.monsterid',
                 'wavatar'   => 'picture.gravatar.wavatar',
                 'retro'     => 'picture.gravatar.retro',
                 'mm'        => 'picture.gravatar.mm',
                 ''          => 'picture.gravatar.gravatar'
                 );
  }
}
