<?php

namespace FSpires\CommitKeeperBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use FSpires\CommitKeeperBundle\Security\FormLoginFactory;

class CKBundle extends Bundle
{
  public function build(ContainerBuilder $container)
  {
    parent::build($container);
    
    $sec_ext = $container->getExtension('security');
    $sec_ext->addSecurityListenerFactory(new FormLoginFactory());
  }
}
