<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType;
use FSpires\CommitKeeperBundle\Model\DateFormatInterface;
use FSpires\CommitKeeperBundle\Form\Type\AttachmentInput;

/**
 * FSpires\CommitKeeperBundle\Form\Type\CommitmentNew
 *
 */
class CommitmentNew extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('title', 'text', array('max_length'=>200, 'label'=>'Title'));

    $fb->add('newDueDate', 'date_picker',
             array('dateFormat'=>$options['dateFormat'],
                   'attr'=>array('class'=>'date_input_new'),
                   'invalid_message' => 'commitment.dateInvalid'));

    if ($options['hasBudgetField']) {
      $fb->add('newBudget', null, array('required'=>false));
    }

    if (!isset($options['reqType'])) {
      throw new \InvalidArgumentException('Option reqType must be set');
    }

    switch($options['reqType']) {
    case RequestType::Request:
      $fb->add('performer', 'user_list', array(
        'choice_list' => new ObjectChoiceList($options['contacts'], 'NameAndEmail', array(), null, 'Id'),
        'empty_value'=>'performer.none.click'
      ));
      break;
    case RequestType::Offer:
      $fb->add('requestor', 'user_input', array('contacts'=>$options['contacts']));
      break;
    case RequestType::ToDo:
      break;
    default:
      throw new \InvalidArgumentException('Invalid value for option reqType: '
                                          . $options['reqType']);
    }

    if ($options['canEditObservers']) {
      $fb->add('observers', 'user_list', array(
             'fixed'       => $options['observersFixed'],
             'choice_list' => $options['observerChoiceList'],
             'empty_value'=>'observers.none.click'
                                                   ));
    }

    if ($options['canEditCategoryValues']) {
      $fb->add('categories', 'ccategory',
               array('options'=>array('empty_value'=>'tag.choose.one'),
                     'choices'=> $options['userCategories'],
                     'required' => false,
                     'error_bubbling' => false
                     ));
    }

    $fb->add('description', 'textarea');
    $fb->add('CCme','checkbox', array('label'=>'CCme', 'required'=>false));
    $fb->add('newAttachments', 'collection',
             array('label'=> 'Attachments.label',
                   'type' => new AttachmentInput(),
                   'allow_add' => true,
                   'prototype' => true
                   )
             );
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
                   'reqType' => 'request',
                   'hasBudgetField' => false,
                   'contacts' => null,
                   'observersFixed' => null,
                   'observerChoiceList' => null,
                   'canEditObservers' => true,
                   'canEditCategoryValues' => true,
                   'userCategories' => null,
                   'data_class' => 'FSpires\CommitKeeperBundle\Entity\Request',
                   'dateFormat' => \IntlDateFormatter::MEDIUM
                                 ));
  }

  public function getName()
  {
    return 'commitmentNew';
  }
}
