<?php
namespace FSpires\CommitKeeperBundle\Form\ChoiceList;

use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityLoaderInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Doctrine\ORM\NativeQuery;
use Doctrine\DBAL\Connection;

/**
 * Getting Entities through the ORM NativeQueryBuilder
 */
class ORMNativeQueryLoader implements EntityLoaderInterface
{
  const PlaceHolder = '@whereClause@';

  /**
   * Alias for the table with the identity column
   * used in the SQL SELECT statment
   *
   * @var string $alias
   */
  private $alias;

  /**
   * SQL string with a placeholder for an additional where clause
   *
   * @var string $sqlWithWherePlaceholder
   */
  private $sqlWithWherePlaceholder;

  /**
   * Contains the native query for fetching the entities
   *
   * @var \Doctrine\ORM\NativeQuery nativeQuery
   */
  private $nativeQuery;

  /**
   * Construct an ORM Query Builder Loader
   *
   * @param \Doctrine\ORM\NativeQuery $nativeQuery
   * @param string $alias Main table alias
   * @param string $altSQL
   */
  public function __construct(NativeQuery $nativeQuery, $alias=null, $altSQL=null)
  {
    $this->nativeQuery = $nativeQuery;
    $this->alias = $alias;
    if ($altSQL) {
      $this->sqlWithWherePlaceholder = $altSQL;
    } else {
      $this->makeSQLwithWherePlaceholder($nativeQuery->getSQL());
    }
  }

  /**
   * Make a query with a placeholder for an additional where clause
   * TODO: This method can be improved, just adhoc for now
   */
  protected function makeSQLwithWherePlaceholder($sql) {
    $wherePos = stripos($sql, 'WHERE ');
    if ($wherePos > 1) {
      // Just remove everything after the WHERE
      $this->sqlWithWherePlaceholder = substr($sql, 0, $wherePos+6)
        . self::PlaceHolder;
    } else {
      // Just add a WHERE. (Will fail if the query contains ORDER but no WHERE.)
      $this->sqlWithWherePlaceholder = $sql . ' WHERE ' . self::PlaceHolder;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getEntities(): array
  {
    return $this->nativeQuery->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function getEntitiesByIds(string $identifier, array $values): array
  {
    if ($this->alias) {
      $sqlIdCol = $this->alias . '.' . $identifier;
    } else {
      $sqlIdCol = $identifier;
    }

    $parameter = 'ORMNativeQueryLoader_getEntitiesByIds_'.$identifier;
    $whereClause = $sqlIdCol . ' IN (:' . $parameter . ')';
    $sql = str_replace(self::PlaceHolder, $whereClause, $this->sqlWithWherePlaceholder);

    $nq = clone ($this->nativeQuery);
    $nq->setSQL($sql);
    $nq->setParameter($parameter, $values, Connection::PARAM_STR_ARRAY);
    return $nq->execute();
  }
}
