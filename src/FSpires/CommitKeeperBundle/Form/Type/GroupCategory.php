<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FSpires\CommitKeeperBundle\Form\Type\Tag;

class GroupCategory extends AbstractType
{
    public function buildForm(FormBuilderInterface $fb, array $options)
    {
      $fb->add('name', null, array('required'=>false,
                                   'label'=>'settings.categories.category'));
      $fb->add('order', 'integer', array('required'=>false,
                                         'label'=>'settings.categories.order'));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\GroupCategory'
                                   ));
    }

    public function getName()
    {
        return 'group_category';
    }
}
