<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use FSpires\CommitKeeperBundle\Form\EventListener\TagReadOnlySubscriber;

class Tag extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $fb, array $options)
    {
      if ($options['hasReadOnly']) {
        $subscriber = new TagReadOnlySubscriber($fb->getFormFactory());
        $fb->addEventSubscriber($subscriber);
        $fb->add('id', 'hidden');
      }
      $fb->add('name');
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
      $tag = $form->getData();
      if ($tag) {
        $view->vars['tagid'] = $tag->getId();
        $view->vars['source'] = $tag->getSource();
      }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\Tag',
          'hasReadOnly' => false
                                   ));
    }

    public function getName()
    {
        return 'tag';
    }
}
