<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * AutoComplete Entity
 */
class AutocompleteEntity extends EntityType
{
  public function getParent(): string
  {
    return 'autocomplete_choice';
  }

  public function getName(): string
  {
    return 'autocomplete_entity';
  }
}
