<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

/**
 * Special input class for a list of default gravatars
 */
class GravatarDefault extends AbstractType
{
  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array('expanded'=>true, 'url' => null));
  }

  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    $view->vars['url'] = $options['url'];
  }

  public function getParent()
  {
    return 'choice';
  }

  public function getName()
  {
    return 'gravatar_default';
  }
}
