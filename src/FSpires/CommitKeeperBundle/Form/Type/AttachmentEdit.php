<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AttachmentEdit extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('delete', 'checkbox', array(
                           'attr' => array('style' => 'display: none'),
                           'required' => false));
    /*
    $fb->add('edit',   'checkbox', array(
                           'attr' => array('style' => 'display: none'),
                           'required' => false));
    $fb->add('name');
    $fb->add('linkUrl', null, array('label'=>'URL'));
    */
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\Attachment'
                                 ));
  }

  public function getName()
  {
    return 'attachmentEdit';
  }
}
