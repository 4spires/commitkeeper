<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AttachmentInput extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('name');
    $fb->add('islink', 'checkbox', array(
                           'attr' => array('style' => 'display: none'),
                           'required' => false));
    $fb->add('linkUrl', null, array('label'=>'URL'));
    $fb->add('file', 'file');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
          'data_class' => 'FSpires\CommitKeeperBundle\Entity\Attachment'
                                 ));
  }

  public function getName()
  {
    return 'attachment';
  }
}
