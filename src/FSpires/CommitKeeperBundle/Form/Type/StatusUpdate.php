<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StatusUpdate extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    $view->vars['choices'] = $options['choices'];
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array('choices'=>null,
                                 'compound'=>false));
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'statusUpdate';
  }
}
