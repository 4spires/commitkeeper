<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AdvChoiceType extends AbstractType
{
  private $translator;
  public function __construct(TranslatorInterface $translator)
  {
    $this->translator = $translator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    if ($options['expanded']) {
      $this->addSubForms($fb, $options['choice_list']->getPreferredViews(), $options);
      $this->addSubForms($fb, $options['choice_list']->getRemainingViews(), $options);
    }
  }

  /**
   * Adds the sub fields for an expanded choice field.
   *
   * @param FormBuilderInterface $builder     The form builder.
   * @param array                $choiceViews The choice view objects.
   * @param array                $options     The build options.
   */
  protected function addSubForms(FormBuilderInterface $builder, array $choiceViews, array $options)
  {
    foreach ($choiceViews as $i => $choiceView) {
      if (is_array($choiceView)) {
        // Flatten groups
        $this->addSubForms($builder, $choiceView, $options);
      } else {
        $choiceOpts = array();
        $choiceOpts['value'] = $choiceView->value;
        $choiceOpts['label'] = $choiceView->label;
        $choiceOpts['translation_domain'] = $options['translation_domain'];
        
        // specific for action choices
        $trLabel = $this->translator->trans(
                          $choiceView->label, array(), 'presenttense');
        $autoText = $this->translator->trans(
                          $choiceView->label, array(), 'autotext');
        $choiceOpts['attr'] =
          array('onclick'=>"change_action(this.value,'"  .
                htmlspecialchars($trLabel, ENT_QUOTES) . "','" .
                htmlspecialchars($autoText, ENT_QUOTES) . "');");

        if ($options['multiple']) {
          $choiceType = 'checkbox';
          $choiceOpts['required'] = false;
        } else {
          $choiceType = 'radio';
        }
        $builder->add((string) $i, $choiceType, $choiceOpts);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'choice';
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'Achoice';
  }
}
