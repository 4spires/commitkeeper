<?php
namespace FSpires\CommitKeeperBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;
use Symfony\Component\Form\Extension\Core\DataTransformer\ChoiceToValueTransformer;
use FSpires\CommitKeeperBundle\Form\DataMapper\UserDataMapper;

/**
 * Input a user with autocomplete or input a totally new user
 */
class UserInput extends AbstractType
{
  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('user', 'user_name_input');
    $fb->add('id', 'hidden');

    $fb->setDataMapper(new UserDataMapper($options['contacts']));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
                   'compound' => true,
                   'contacts' => null,
                   'acoptions' => array(
                        'width'       => '400',
                        'minChars'    => 1,
                        'delay'       => 5,
                        'selectFirst' => 'true',
                        'matchSubset' => 1,
                        'autoFill'    => 'true',
                        'matchCase'   => 0,
                        'maxItemsToShow' => 15
                                        ),
                   'data_class' => 'FSpires\CommitKeeperBundle\Entity\UserName',
                   'error_bubbling' => false
                                 ));
  }

  /**
   * {@inheritdoc}
   */
  public function buildView(FormView $view, FormInterface $form, array $options)
  {
    // Transform the contacts to an array for the autocomplete javascript
    $choices = array();
    foreach ($options['contacts'] as $contact) {
      $choices[] = array('',
                         $contact->getId(),
                         $contact->getFirstName(),
                         $contact->getLastName(),
                         $contact->getEmail());
    }
    $choices = json_encode($choices);

    $ac_options = $options['acoptions'];
    // Transform the ac_options array for use in the javascript
    $ac_options = json_encode($ac_options, JSON_FORCE_OBJECT);

    $view->vars['choices'] = $choices;
    $view->vars['ac_options'] = $ac_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getParent()
  {
    return 'form';
  }

  /**
   * {@inheritdoc}
   */
  public function getName()
  {
    return 'user_input';
  }
}
