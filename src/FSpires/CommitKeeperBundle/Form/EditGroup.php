<?php
namespace FSpires\CommitKeeperBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager as EntityManagerInterface;
use FSpires\CommitKeeperBundle\Form\Type\GroupCategory;
use Symfony\Component\Form\Extension\Core\ChoiceList\ObjectChoiceList;

class EditGroup extends AbstractType
{
  /**
   * The Doctrine Entity Manager Object used to connect to the database
   * @var \Doctrine\ORM\EntityManager
   */
  private $em;

  /**
   * Constructor
   *
   * @param EntityManagerInterface $em Entity Manager
   */
  public function __construct(EntityManagerInterface $em) {
    $this->em = $em;
  }

  public function buildForm(FormBuilderInterface $fb, array $options)
  {
    $fb->add('categories', 'collection',
             array('label'=>'settings.categories.name',
                   'type'=> new GroupCategory(),
                   'allow_add' => true,
                   'prototype' => true,
                   'error_bubbling' => false
                   ));

    $fb->add('hasBudgetField', null,
             array('label'=>'settings.displayBudget',
                   'required' => false));

    $budgetUnitRepo = $this->em->getRepository('CKBundle:BudgetUnit');
    $units = $budgetUnitRepo->findAll();
    $preferred = array();
    foreach ($units as $unit) {
      if ($unit->isPreferred()) {
        $preferred[] = $unit;
      }
    }
    $choice_list = new ObjectChoiceList($units, 'Unit', $preferred, null, 'Id');
    $fb->add('budgetUnit', 'sep_choice', array('choice_list' => $choice_list, 'label' => 'settings.units'));
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
         'data_class' => 'FSpires\CommitKeeperBundle\Entity\Group'
                                 ));
  }

  public function getName()
  {
    return 'edit_group';
  }
}
