<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class Supporting extends Selection
{
  private $request;

  public function __construct($repository, $request) {
    parent::__construct($repository);
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlSupporting();
    $this->initDone = true;
  }


  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->request->getSupportingRequests();
  }

  /**
   * {@inheritdoc}
   */
  public function getCategories($userId, $groupId) {
    if (!$this->initDone) {
      $this->initSelection();
    }
    return $this->repository->getCategoriesWithTags($this->condition_sql, $this->joinObservers, false, $groupId, $this->request->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function getUnread($userId) {
    if (!$this->initDone) {
      $this->initSelection();
    }
    return $this->repository->getUnread($this->condition_sql, $this->joinObservers, $userId, $this->request->getId());
  }
}
