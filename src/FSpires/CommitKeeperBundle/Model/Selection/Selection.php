<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

abstract class Selection implements SelectionInterface
{
  /**
   * @var \FSpires\CommitKeeperBundle\Entity\RequestRepository
   */
  protected $repository;

  /**
   * @var string
   */
  protected $condition_sql;

  /**
   * @var bool
   */
  protected $joinObservers;

  /**
   * @var bool
   */
  protected $initDone = false;

  public function __construct($repository) {
    $this->repository = $repository;
  }

  /**
   * Set the class variables for this selection
   * Must set $this->initDone = true;
   */
  abstract protected function initSelection();

  /**
   * {@inheritdoc}
   */
  public function getCategories($userId, $groupId) {
    if (!$this->initDone) {
      $this->initSelection();
    }
    return $this->repository->getCategoriesWithTags($this->condition_sql, $this->joinObservers, $userId, $groupId);
  }

  /**
   * {@inheritdoc}
   */
  public function getUnread($userId) {
    if (!$this->initDone) {
      $this->initSelection();
    }
    return $this->repository->getUnread($this->condition_sql, $this->joinObservers, $userId);
  }

  /**
   * Get any additonal template settings
   * related to this selection
   * @param string $prefix Prefix used on template vars
   * @return array of template variables
   */
  public function getTemplateVars($prefix) {
    return array();
  }
}
