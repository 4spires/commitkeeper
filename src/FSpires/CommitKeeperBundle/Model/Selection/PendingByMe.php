<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class PendingByMe extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlPendingByMe();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findPendingByMe($userId, $orderby);
  }
}
