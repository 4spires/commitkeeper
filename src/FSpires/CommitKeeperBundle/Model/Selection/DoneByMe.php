<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

class DoneByMe extends Selection
{
  /**
   * {@inheritdoc}
   */
  protected function initSelection() {
    list ($this->condition_sql, $this->joinObservers) = $this->repository->getSqlByPerformer();
    $this->initDone = true;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommitments($userId, $orderby) {
    return $this->repository->findByPerformer($userId, $orderby);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplateVars($prefix) {
    return array($prefix.'DispPerformer' => false);
  }
}
