<?php
namespace FSpires\CommitKeeperBundle\Model\Selection;

interface SelectionInterface
{
  /**
   * Get the commitments for this selection
   */
  public function getCommitments($userId, $orderby);

  /**
   * Get the categories and tags for this selection
   */
  public function getCategories($userId, $groupId);

  /**
   * Get which commitments are unread for this selection
   */
  public function getUnread($userId);

  /**
   * Get any additonal template settings
   * related to this selection
   * @param string $prefix Prefix used on template vars
   * @return array of template variables
   */
  public function getTemplateVars($prefix);
}
