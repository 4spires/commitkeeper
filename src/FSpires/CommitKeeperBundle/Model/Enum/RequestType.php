<?php
namespace FSpires\CommitKeeperBundle\Model\Enum;

/**
 * A class to enumerate the different request types
 */
final class RequestType
{
  private function __construct(){}

  const Request = 'request';
  const Offer = 'offer';
  const ToDo = 'toDo';

  static public function getAll() {
    return array (
      self::Request,
      self::Offer,
      self::ToDo
    );
  }
}
