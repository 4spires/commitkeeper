<?php
namespace FSpires\CommitKeeperBundle\Model\Enum;

/**
 * A class to enumerate the different phases of a request
 */
final class Phase
{
  private function __construct(){}

  const Preparation = 'Preparation';
  const Negotiation = 'Negotiation';
  const Delivery = 'Delivery';
  const Acknowledge = 'Acknowledge';
  const Closed = 'Closed';
}
