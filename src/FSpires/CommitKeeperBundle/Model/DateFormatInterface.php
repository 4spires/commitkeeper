<?php
namespace FSpires\CommitKeeperBundle\Model;

/**
 * A interface that gives out the date format to be used
 * in all of the the application
 */
interface DateFormatInterface
{
  /**
   * Get the IntlDateFormatter date format
   */
  public function getIDFformat();

  /**
   * Get a ICU type (http://userguide.icu-project.org/formatparse/datetime)
   * date pattern
   */
  public function getICUpattern();

  /**
   * Get a PHP date type date pattern
   */
  public function getPHPpattern();

  /**
   * Get a date pattern for DatePicker
   */
  public function getDPpattern();

  /**
   * Get template variables for the DatePicker
   */
  public function getDatePickerVars($date, $id);
}
