<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Model\Selection\SelectionInterface;

interface RequestTableInterface
{
  /**
   * Get data to display a table of requests
   *
   * @param int         $userId     Id of the user the table is for
   * @param int         $groupId    Id of the group the user is in,
   *                                for sorting categories of tags
   * @param SelectionInterface $selection  Get only requests from this selection
   * @param bool|string $sortcol    Initial sorting by this column
   * @param string      $prefix     Prefix in the keys in the returned array
   * @return array
   */
  public function getData($userId, $groupId, SelectionInterface $selection, $sortcol=false, $prefix='');
}
