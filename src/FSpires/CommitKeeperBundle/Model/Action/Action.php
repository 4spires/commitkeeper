<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

/**
 * Abstract base class for all the actions
 */
abstract class Action implements ActionInterface
{
  private $label;

  protected $actor;
  protected $reqType;

  public function __construct($label, $actor, $reqType)
  {
    $this->label = $label;
    $this->actor = $actor;
    $this->reqType = $reqType;
  }

  /**
   * Get the label for the action used in the UI
   * This label will be translated later
   */
  public function getLabel()
  {
    return $this->label;
  }

  /**
   * Get the role of the actor in this action
   * Either 'R', 'P', or 'O'.
   */
  public function getActorRole()
  {
    return $this->actor;
  }

  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    /* This function body is intentionally left blank */
    /* The default is not to change the request */
  }
}
