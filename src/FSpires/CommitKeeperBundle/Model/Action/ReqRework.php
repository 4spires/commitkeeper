<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * The Request rework action
 */
class ReqRework extends Action
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    // Set the next phase and the traffic light
    // and set who it is pending by next
    $request->setPhase(Phase::Delivery);
    $request->setTrafficLight(TL::OnTrack);
    $request->setPhasePendingBy('P');
  }
}
