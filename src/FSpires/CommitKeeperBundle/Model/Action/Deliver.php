<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * This action class covers Deliver and Resubmit actions
 */
class Deliver extends Action
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    // Set the next phase and the traffic light
    // and set who it is pending by next
    $request->setPhase(Phase::Acknowledge);
    $request->setTrafficLight(TL::Delivered);
    $request->setPhasePendingBy('R');
  }
}
