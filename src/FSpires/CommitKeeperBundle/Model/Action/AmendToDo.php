<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * This action class is also base class for
 * Amend, Counter and Revise actions
 */
class AmendToDo extends Action
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    // Set red traffic ligth to green if date is pushed out (Issue 321)
    if ($request->getTrafficLight()->getId()==TL::OffTrack) {
      $oldDate = $request->getDueDate();
      $dueDate = $request->getNewDueDate();
      if ($oldDate < $dueDate) {
        $today = new \DateTime('today');
        if ($oldDate < $today && $today < $dueDate) {
          $request->setTrafficLight(TL::OnTrack);
        }
      }
    }

    $this->updateAmendCommon($request);
  }

  /**
   * Update things common for all "Amend"s
   */
  protected function updateAmendCommon($request)
  {
    // Update the due date and the budget
    $request->updateDueDate();
    $request->updateBudget();
  }
}
