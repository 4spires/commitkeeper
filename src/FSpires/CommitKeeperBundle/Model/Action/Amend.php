<?php
namespace FSpires\CommitKeeperBundle\Model\Action;

use FSpires\CommitKeeperBundle\Model\Enum\Phase;
use FSpires\CommitKeeperBundle\Model\Enum\RequestType as RT;
use FSpires\CommitKeeperBundle\Model\Enum\TrafficLight as TL;

/**
 * This action class covers Counter, Revise and Amend actions
 */
class Amend extends AmendToDo
{
  /**
   * Update the request object the action is done on
   */
  public function updateRequest($request)
  {
    //Update things common with "To Do"s (like the due date)
    $this->updateAmendCommon($request);

    // Set the next phase and the traffic light
    if ($request->getPhase() != Phase::Negotiation) {
      $request->setPhase(Phase::Negotiation);
      $request->setTrafficLight(TL::Negotiation);
    }

    // Set who it is pending by next
    $next = 'P';
    if ('P'==$this->actor) {
      $next = 'R';
    }
    $request->setPhasePendingBy($next);
  }
}
