<?php
namespace FSpires\CommitKeeperBundle\Model;

use FSpires\CommitKeeperBundle\Entity\UserBase;
use FSpires\CommitKeeperBundle\Entity\Request;
use FSpires\CommitKeeperBundle\Entity\History;
use FSpires\CommitKeeperBundle\Entity\User;

/**
 * Interface for class responsible for email notifications
 */
interface EmailNotifierInterface
{
  /**
   * Notify by email the others users about the action taken
   */
  public function notify(UserBase $user, Request $req, History $hist);

  /**
   * Send email to a new user that has just been created.
   */
  public function newUser(User $new_user, UserBase $creator);

  /**
   * Send a password reset email
   * to a user that has forgotten the password.
   */
  public function sendPasswordReset(UserBase $user, $tmpPassword, $expiry_date);

  /**
   * Send a system notification that a new user
   * has logged in for the first time
   */
  public function firstLogin(UserBase $user);

  /**
   * Send a daily report about the commitments of the user
   */
  public function dailyReport(UserBase $user, RequestTableInterface $requestTable);
}
