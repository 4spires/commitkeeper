<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FabienController extends AbstractController
{
    public function __construct(
        private readonly string $webhost
    ) {
    }

    #[Route('/', name: 'app_fabien')]
    public function index(): Response
    {
        return $this->render('fabien/index.html.twig', [
            'controller_name' => 'FabienController',
            'host' => $this->webhost,
        ]);
    }
}
