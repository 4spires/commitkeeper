SET NAMES 'utf8' COLLATE 'utf8_danish_ci';

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachment`
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) DEFAULT NULL,
  `updated_date` datetime NOT NULL,
  `islink` tinyint(1) NOT NULL,
  `name` varchar(200) NOT NULL,
  `url` varchar(300) NOT NULL,
  `orig_file_name` varchar(250) DEFAULT NULL,
  `content_type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ATT_REQ` (`request_id`),
  CONSTRAINT `FK_ATT_REQ` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`)
) ENGINE=InnoDB;


--
-- Table structure for table `picture`
--

DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `has_file` tinyint(1) NOT NULL,
  `url` varchar(300) NOT NULL,
  `content_type` varchar(200) NOT NULL,
  `gravatar_default` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PIC_USER` (`user_id`),
  CONSTRAINT `FK_PIC_USER` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;

--
-- Table structure for table `budget_field_unit`
--

DROP TABLE IF EXISTS `budget_field_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_field_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(25) COLLATE utf8_danish_ci NOT NULL,
  `preferred` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_field_unit`
--

LOCK TABLES `budget_field_unit` WRITE;
/*!40000 ALTER TABLE `budget_field_unit` DISABLE KEYS */;
INSERT INTO `budget_field_unit` VALUES (1,'Hours',1),(2,'Days',1),(3,'Weeks',1),(4,'$',0),(5,'€',0),(6,'£',0);
/*!40000 ALTER TABLE `budget_field_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'No category'),(2,'Project'),(3,'Account'),(4,'Goal');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cgroup`
--

DROP TABLE IF EXISTS `cgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `has_budgetf` boolean NOT NULL,
  `budgetf_unit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BUDGET_F` (`budgetf_unit_id`),
  CONSTRAINT `FK_BUDGET_F` FOREIGN KEY (`budgetf_unit_id`) REFERENCES `budget_field_unit` (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cgroup`
--

LOCK TABLES `cgroup` WRITE;
/*!40000 ALTER TABLE `cgroup` DISABLE KEYS */;
INSERT INTO `cgroup` VALUES (1,'Default group',0,1);
/*!40000 ALTER TABLE `cgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cgroup_category`
--

DROP TABLE IF EXISTS `cgroup_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cgroup_category` (
  `cgroup_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `corder` int(11) NOT NULL,
  PRIMARY KEY (`cgroup_id`,`category_id`),
  KEY `IDX_GROUP_C` (`cgroup_id`),
  KEY `IDX_CATEGORY_G` (`category_id`),
  CONSTRAINT `FK_CATEGORY_G` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `FK_GROUP_C` FOREIGN KEY (`cgroup_id`) REFERENCES `cgroup` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cgroup_category`
--

LOCK TABLES `cgroup_category` WRITE;
/*!40000 ALTER TABLE `cgroup_category` DISABLE KEYS */;
INSERT INTO `cgroup_category` VALUES (1,2,1),(1,3,2),(1,4,3);
/*!40000 ALTER TABLE `cgroup_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `revision` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `role` char(1) NOT NULL,
  `action` varchar(25) NOT NULL,
  `phase` varchar(20) NOT NULL,
  `phase_changed` tinyint(1) NOT NULL,
  `traffic_light` int(11) NOT NULL,
  `traffic_light_changed` tinyint(1) NOT NULL,
  `due_date` date NOT NULL,
  `due_date_previous` date,
  `budget` varchar(25),
  `budget_previous` varchar(25),
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_REQUEST` (`request_id`),
  KEY `FK_ACTOR` (`actor_id`),
  KEY `FK_TLIGHT` (`traffic_light`),
  CONSTRAINT `FK_ACTOR` FOREIGN KEY (`actor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_REQUEST` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_TLIGHT` FOREIGN KEY (`traffic_light`) REFERENCES `traffic_light` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `openIdentifier`
--

DROP TABLE IF EXISTS `openIdentifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `openIdentifier` (
  `identifier` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `provider` varchar(100) NOT NULL,
  PRIMARY KEY (`identifier`),
  KEY `IDX_D8AC7076A76ED395` (`user_id`),
  CONSTRAINT `FK_D8AC7076A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rememberme_token`
--

DROP TABLE IF EXISTS `rememberme_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rememberme_token` (
  `series` char(88) NOT NULL,
  `value` char(88) NOT NULL,
  `class` char(38) NOT NULL,
  `lastUsed` datetime NOT NULL,
  `username` varchar(200) NOT NULL,
  PRIMARY KEY (`series`),
  UNIQUE KEY `series` (`series`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `traffic_light` int(11) NOT NULL,
  `phase` varchar(20) NOT NULL,
  `phase_pending_by` char(1) NOT NULL,
  `initiator_id` int(11) NOT NULL,
  `priority` varchar(10) NOT NULL,
  `requestor_id` int(11) NOT NULL,
  `performer_id` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `has_budgetf` boolean NOT NULL,
  `budget` varchar(25),
  `budgetf_unit_id` int(11) NOT NULL,
  `super_request_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_TL` (`traffic_light`),
  KEY `IDX_INIT` (`initiator_id`),
  KEY `IDX_REQ` (`requestor_id`),
  KEY `IDX_PERF` (`performer_id`),
  KEY `IDX_SUPER` (`super_request_id`),
  KEY `IDX_R_BUDGET_F` (`budgetf_unit_id`),
  CONSTRAINT `FK_INIT` FOREIGN KEY (`initiator_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_PERF` FOREIGN KEY (`performer_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_REQ` FOREIGN KEY (`requestor_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_SUPER` FOREIGN KEY (`super_request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_TL` FOREIGN KEY (`traffic_light`) REFERENCES `traffic_light` (`id`),
  CONSTRAINT `FK_R_BUDGET_F` FOREIGN KEY (`budgetf_unit_id`) REFERENCES `budget_field_unit` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_unread`
--

DROP TABLE IF EXISTS `request_unread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_unread` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`request_id`,`user_id`),
  KEY `IDX_REQUEST_UR` (`request_id`),
  KEY `IDX_USER_UR` (`user_id`),
  CONSTRAINT `FK_REQUEST_UR` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_USER_UR` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_observer`
--

DROP TABLE IF EXISTS `request_observer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_observer` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`request_id`,`user_id`),
  KEY `IDX_REQUEST_O` (`request_id`),
  KEY `IDX_USER_R` (`user_id`),
  CONSTRAINT `FK_REQUEST_O` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_USER_R` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_parentobserver`
--

DROP TABLE IF EXISTS `request_parentobserver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_parentobserver` (
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`request_id`,`user_id`),
  KEY `IDX_REQUEST_PO` (`request_id`),
  KEY `IDX_PUSER_R` (`user_id`),
  CONSTRAINT `FK_REQUEST_PO` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_PUSER_R` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_tag`
--

DROP TABLE IF EXISTS `request_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_tag` (
  `request_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`request_id`,`tag_id`),
  KEY `IDX_REQUEST_T` (`request_id`),
  KEY `IDX_TAG_R` (`tag_id`),
  CONSTRAINT `FK_REQUEST_T` FOREIGN KEY (`request_id`) REFERENCES `request` (`id`),
  CONSTRAINT `FK_TAG_R` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT 1,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CATEGORY_T` (`category_id`),
  CONSTRAINT `FK_CATEGORY_T` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES
 (1,2,'Project 1'), (2,2,'Project 2'), (3,2,'Project 3'),
 (4,3,'Account 1'), (5,3,'Account 2'), (6,3,'Account 3'),
 (7,4,'Goal 1'), (8,4,'Goal 2'), (9,4,'Goal 3');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_password`
--

DROP TABLE IF EXISTS `temp_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `expiry_date` datetime NOT NULL,
  `used` boolean NOT NULL,
  `password` varchar(60) NOT NULL,
  `ip` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_USER_TP` (`user_id`),
  CONSTRAINT `FK_USER_TP` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `traffic_light`
--

DROP TABLE IF EXISTS `traffic_light`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `traffic_light` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `filename` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `traffic_light`
--

LOCK TABLES `traffic_light` WRITE;
/*!40000 ALTER TABLE `traffic_light` DISABLE KEYS */;
INSERT INTO `traffic_light` VALUES
 ( 1,'Negotiation','negotiation.png'),
 ( 2,'Off track','off_track.png'),
 ( 3,'At risk','at_risk.png'),
 ( 4,'On track','on_track.png'),
 ( 5,'Delivered','delivered.png'),
 ( 6,'Completed','completed.png'),
 ( 7,'Canceled request','cancelled.png'),
 ( 8,'Canceled agreement','cancelled.png'),
 ( 9,'Canceled request','cancelled.png'),
 (10,'Canceled offer','cancelled.png'),
 (11,'Declined request','declined.png'),
 (12,'Declined offer','declined.png'),
 (13,'Canceled counter','cancelled.png'),
 (14,'Canceled counter','cancelled.png'),
 (15,'Declined counter','declined.png'),
 (16,'Declined counter','declined.png'),
 (17,'Canceled to do','cancelled.png');

/*!40000 ALTER TABLE `traffic_light` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_user_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(60) NOT NULL,
  `password_expiry_date` datetime NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `user_level` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `user_owner_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '1',
  `cgroup_id` int(11) NOT NULL DEFAULT '1',
  `language` VARCHAR(2) NOT NULL DEFAULT 'en',
  `time_zone_offset` smallint(3) NOT NULL,
  `time_zone_slot` tinyint(2) NOT NULL,
  `daily_report_enabled` boolean NOT NULL DEFAULT 1,
  `hasWelcomeScreen` boolean NOT NULL,
  `ccme` boolean NOT NULL DEFAULT 1,
  `active` boolean NOT NULL DEFAULT 1,
  `default_date_interval` varchar(12) NOT NULL DEFAULT 'P1W',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `IDX_GROUP_U` (`cgroup_id`),
  KEY `IDX_TIME_ZONE_SLOT` (`time_zone_slot`),
  KEY `IDX_DAILY_REPORT` (`daily_report_enabled`),
  KEY `IDX_ACTIVE` (`active`),
  CONSTRAINT `FK_GROUP_U` FOREIGN KEY (`cgroup_id`) REFERENCES `cgroup` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,'4Spires','Support','support@4spires.com','noNif4jnZkg7A','0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00',5,3,1,1,1,'en',0,0,1,1,1,1,'P1W');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_contact`
--

DROP TABLE IF EXISTS `user_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_contact` (
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`contact_id`),
  KEY `IDX_USER_CONTACT` (`user_id`),
  KEY `IDX_CONTACT_USER` (`contact_id`),
  CONSTRAINT `FK_CONTACT_USER` FOREIGN KEY (`contact_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_USER_CONTACT` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_tag`
--

DROP TABLE IF EXISTS `user_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_tag` (
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`tag_id`),
  KEY `IDX_USER_T` (`user_id`),
  KEY `IDX_TAG_U` (`tag_id`),
  CONSTRAINT `FK_TAG_U` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`),
  CONSTRAINT `FK_USER_T` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_tag`
--

LOCK TABLES `user_tag` WRITE;
/*!40000 ALTER TABLE `user_tag` DISABLE KEYS */;
INSERT INTO `user_tag` VALUES
 (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9);
/*!40000 ALTER TABLE `user_tag` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
